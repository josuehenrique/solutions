class AddFieldJustificationForTeam < ActiveRecord::Migration
  def change
    add_column :teams, :justification, :text, null: false
  end
end
