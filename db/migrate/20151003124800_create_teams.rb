class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name, null: false, limit: 150
      t.boolean :agree_with_terms, null: false, default: 0

      t.timestamps
    end
  end
end
