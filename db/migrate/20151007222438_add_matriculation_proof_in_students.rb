class AddMatriculationProofInStudents < ActiveRecord::Migration
  def change
    add_attachment :students, :matriculation_proof
  end
end
