class ChangeTypeFieldPeriod < ActiveRecord::Migration
  def change
    change_column :students, :period, :string, limit: 8
  end
end
