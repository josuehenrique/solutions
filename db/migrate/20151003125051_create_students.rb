class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :name, null: false, limit: 150
      t.integer :registration, null: false
      t.string :kind, null: false, limit: 3
      t.boolean :leader, null: false, default: 0
      t.integer :period, null: false
      t.string :email, null: false, limit: 150
      t.string :phone_number, null: false, limit: 11
      t.integer :team_id, null: false, index: true

      t.timestamps
    end
    add_foreign_key :students, :team_id, :teams
  end
end
