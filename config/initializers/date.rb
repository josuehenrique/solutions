class Date
  def holiday?(business_unit_id)
    relation = BusinessUnitRelation.arel_table

    Holiday.
      joins(:business_unit_relations).
      where(date: self).
      where(relation[:business_unit_id].eq(business_unit_id)).any?
  end
end
