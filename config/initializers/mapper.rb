module ActionDispatch
  module Routing
    class Mapper
      module Resources
        def resources(*resources, &block)
          options = resources.extract_options!.dup

          if apply_common_behavior_for(:resources, resources, options, &block)
            return self
          end

          resource_scope(:resources, Resource.new(resources.pop, options)) do
            yield if block_given?

            collection do
              get :index if parent_resource.actions.include?(:index)
              post :create if parent_resource.actions.include?(:create)
            end

            new do
              get :new
            end if parent_resource.actions.include?(:new)

            member do
              get :edit if parent_resource.actions.include?(:edit)
              get :show if parent_resource.actions.include?(:show)
              put :update if parent_resource.actions.include?(:update)
              delete :destroy if parent_resource.actions.include?(:destroy)

              get :activate
              get :inactivate
            end
          end

          self
        end
      end
    end
  end
end
