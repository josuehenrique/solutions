module ActionView
  module Helpers
    module FormOptionsHelper
      def options_for_select(container, selected = nil)
        return container if String === container

        selected, disabled = extract_selected_and_disabled(selected).map do |r|
          Array(r).map { |item| item.to_s }
        end

        container.map do |element|
          html_attributes = option_html_attributes(element)
          text, value = option_text_and_value(element).map { |item| item.to_s }

          html_attributes[:selected] = 'selected' if option_value_selected?(value, selected)
          html_attributes[:disabled] = 'disabled' if disabled && option_value_selected?(value, disabled)
          html_attributes[:value] = value

          if element.respond_to?(:size) && element.size > 2 && !element.last.blank?
            element.last.each do |key, value|
              html_attributes[key] = value
            end
          end

          content_tag_string(:option, text, html_attributes)
        end.join("\n").html_safe
      end

      private

      def option_text_and_value(option)
        # Options are [text, value] pairs or strings used for both.
        if !option.is_a?(String) && option.respond_to?(:first) && option.respond_to?(:second)
          option = option.reject { |e| Hash === e } if Array === option
          [option.first, option.second]
        else
          [option, option]
        end
      end
    end
  end
end
