require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(:default, Rails.env)

module Solutions
  class Application < Rails::Application
    config.to_prepare { Devise::SessionsController.layout 'login' }

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += %W(
      #{config.root}/lib
      #{config.root}/app/business
      #{config.root}/app/searchers
      #{config.root}/app/validators
    )

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Brasilia'
    config.active_record.default_timezone = :local
    config.active_record.time_zone_aware_attributes = false

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir["#{config.root}/config/locales/**/*.yml"]
    config.i18n.enforce_available_locales = false
    config.i18n.available_locales = ["pt-BR"]
    config.i18n.default_locale = :'pt-BR'

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    # Enable escaping HTML in JSON.
    config.active_support.escape_html_entities_in_json = true

    # Include helpers from current controller only
    config.action_controller.include_all_helpers = false

    config.active_record.disable_implicit_join_references = false

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'

    config.assets.enabled = true

    config.assets.precompile += ['base/jquery-1.9.1.js']

    css_files = []
    Dir.glob("#{config.root}/app/assets/stylesheets/*.{css, erb}").each { |f|
      css_files << File.basename(f)
    }

    js_files = []
    Dir.glob("#{config.root}/app/assets/javascripts/*.{js, erb}").each { |f|
      js_files << File.basename(f)
    }

    config.assets.precompile += css_files.concat(js_files)

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default :charset => 'utf-8'

    config.action_mailer.smtp_settings = {
      address:        'smtp.gmail.com',
      port:           '587',
      user_name:      'project.business.academy@gmail.com',
      password:       'Bus1n3ss@@',
      enable_starttls_auto: true,
      authentication: :plain
    }
  end
end
