require 'bundler/capistrano'

set :application, "solutions"
set :scm, :none
set :repository, "."
set :deploy_via, :copy
set :branch, "master"
set :ssh_options, { forward_agent: true }
ssh_options[:keys] = [ENV["CAP_PRIVATE_KEY"]] if ENV["CAP_PRIVATE_KEY"]
ssh_options[:verbose] = :debug
set :stage, :production
set :user, "probitate"
set :use_sudo, false
set :deploy_to, "/home/probitate/#{application}"
set :domain, "187.17.150.10"
set :keep_releases, 2
set :default_environment, {
  'PATH' => "/home/probitate/.rvm/gems/ruby-2.1.2/bin:/home/probitate/.rvm/bin:/home/probitate/.rvm/ruby-2.1.2@global/bin:$PATH",
  'RUBY_VERSION' => 'ruby 2.1.2',
  'GEM_HOME'     => '/home/probitate/.rvm/gems/ruby-2.1.2',
  'GEM_PATH'  => '/home/probitate/.rvm/gems/ruby-2.1.2:/home/probitate/.rvm/gems/ruby-2.1.2@global',
  'BUNDLE_PATH'  => '/home/probitate/.rvm/gems/ruby-2.1.2:/home/probitate/.rvm/gems/ruby-2.1.2@global'  # If you are using bundler.
}

role :app, domain
role :web, domain
role :db, domain, primary: true

set :assets_role, [:web, :app]
load 'deploy/assets'

before  "deploy:assets:precompile", "bundle:install", "deploy:config_files"
after  "deploy:update_code", "deploy:migrate"

namespace :deploy do
  desc "Copy files"
  task :config_files do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/application.yml #{release_path}/config/application.yml"
  end
end

namespace :passenger do
  desc "Restart Application"
  task :restart do
    run "touch #{current_path}/tmp/restart.txt"
  end
end

namespace :log do
  desc "A pinch of tail"
  task :tailf, roles: :app do
    run "tail -n 10000 -f #{shared_path}/log/#{rails_env}.log" do |channel, stream, data|
      puts "#{data}"
      break if stream == :err
    end
  end
end
