Solutions::Application.routes.draw do
  devise_for :users

  resources :teams, except: [:destroy, :edit, :index] do
    get :new_team, on: :collection
    get :download_regulations, on: :collection
  end

  resources :students

  root to: 'home#index'

  get '*a', to: 'application#render_error' if Rails.env.production?
end
