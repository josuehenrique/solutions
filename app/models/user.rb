class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :timeoutable,
    :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :login, :password, :password_confirmation, :skin,
    :employee_id, :roles_attributes

  attr_protected :administrator

  attr_list :login, :email, :skin, :employee_id

  attr_search :login

  has_enumeration_for :skin

  belongs_to :employee

  has_many :roles, as: :related, dependent: :destroy
  has_many :notifications, dependent: :destroy

  delegate :business_units, :email, :request_queues, :bank_accounts, :cashiers, :stocks,
    to: :employee, allow_nil: true

  accepts_nested_attributes_for :roles, allow_destroy: true

  validates :login, presence: true
  validates :password, presence: true, on: :create

  orderize :id

  def to_s
    login
  end

  def skin_css
    case skin
    when Skin::SKIN1 then
      'skin-1'
    when Skin::SKIN2 then
      'skin-2'
    when Skin::SKIN3 then
      'skin-3'
    else
      'default'
    end
  end

  def build_role(attributes)
    roles.build(attributes)
  end

  def delete_role(role)
    roles.delete(role)
  end

  def email_required?
    false
  end

  def email_changed?
    false
  end
end
