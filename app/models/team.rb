class Team < ActiveRecord::Base
  attr_accessible :name, :agree_with_terms, :leader_id, :students_attributes, :justification

  attr_accessor :token

  has_many :students

  accepts_nested_attributes_for :students,
                                reject_if: proc { |attr| attr['name'].blank? &&
                                  attr['email'].blank? && attr['registration'].blank? &&
                                  attr['period'].blank? && attr['phone_number'].blank? }

  validate :checked_agree_with_terms, :has_one_leader?, :justification_words_size

  validates :name, :justification, presence: true

  orderize

  def checked_agree_with_terms
    if agree_with_terms == false
      errors.add(:base, 'Selecione a opção CONCORDO COM OS TERMOS DO REGULAMENTO')
    end
  end

  def has_one_leader?
    if !students.collect(&:leader).include? true
      errors.add(:base, 'Selecione um LÍDER para a equipe.')
    end
  end

  def justification_words_size
    if justification.split.size >= 250
      errors.add(:base, 'A justificativa deve ser de no máximo 250 palavras.')
    end
  end

  def leader
    students.select(&:leader).first
  end
end
