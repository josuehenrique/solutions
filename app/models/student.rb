class Student < ActiveRecord::Base
  has_enumeration_for :kind, with: StudentKind

  attr_accessible :kind, :phone_number, :name, :period, :team_id,
                  :registration, :email, :leader, :matriculation_proof

  belongs_to :team

  has_attached_file :matriculation_proof,
                    url: "/anexos/:class/:attachment/:id/:style_:basename.:extension",
                    path: ":rails_root/public/anexos/:class/:attachment/:id/:style_:basename.:extension"

  validates_attachment_content_type :matriculation_proof, content_type: /\Aimage/

  validates :registration, uniqueness: true, numericality: { less_than_or_equal_to: 2147483647 }
  validates :phone_number, length: {minimum: 10, maximum: 11}

  validates_format_of :email,:with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/

  def to_s
    "#{registration} - #{name}"
  end

  orderize
end
