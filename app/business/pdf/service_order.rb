module Pdf
  class ServiceOrder < Pdf::Manager
    def initialize(service_order)
      @service_order = service_order
      @client = service_order.client
      @client_plan = service_order.client_plan
      @business_unit = client.business_unit
      @address = client_plan.address

      @width = 525
      @height = 775
      @position = 0
      @image = business_unit.logo.path

      super
    end

    def render
      set_image(pdf, image, 80, 65, 5, 8, :center, :middle)

      header

      line(80)

      body_left
      body_right

      line(188)

      description

      pdf.move_down(40)

      attributes

      pdf.move_down(50)

      products

      bottom

      pdf.stroke_bounds
      pdf.render
    end

    private

    attr_reader :service_order, :business_unit, :client, :client_plan,
      :width, :height, :image, :position, :address

    def body_left
      pdf.bounding_box([4, height - 84], width: (width / 2) -4, height: 102) do
        pdf.text "Número OS: #{service_order.id}"
        pdf.text "Data Atendimento: #{I18n.l service_order.attendance.created_at}"
        pdf.text "Data abertura da OS: #{I18n.l service_order.created_at}"
        pdf.text "Cliente: #{client.name}"
        pdf.text "Telefone: #{Mask.phone(client.phone_number)}"

        if address
          pdf.text "Endereço: #{address.street}, nº #{address.number}"
          pdf.text "Complemento: #{address.complement}"
          pdf.text "Cidade: #{address.city}"
        else
          pdf.text "Endereço: #{client.street}, nº #{client.number}"
          pdf.text "Complemento: #{client.complement}"
          pdf.text "Cidade: #{client.city}"
        end
      end
    end

    def body_right
      pdf.bounding_box([(width / 2) + 2, height - 84], width: (width / 2) -4, height: 102) do
        pdf.text "Técnico: #{service_order.employee}"
        pdf.text "Plano: #{client_plan}"
        pdf.text "Data agendamento: #{service_order.scheduling_date_and_bout}"
        pdf.text "Identificadordo Cliente: #{client.id}"
        pdf.text "Tipo OS: #{service_order.os_type}"

        if address
          pdf.text "Bairro: #{address.district}"
          pdf.text "CEP: #{Mask.zipcode(address.zipcode)}"
        else
          pdf.text "Bairro: #{client.district}"
          pdf.text "CEP: #{Mask.zipcode(client.zipcode)}"
        end
      end
    end

    def description
      pdf.bounding_box([3, height - 192], width: width - 6, height: 78) do
        pdf.text 'Descrição:', style: :bold
        pdf.text service_order.description

        service_order.observations.each do |observation|
          pdf.text "#{observation.employee.name} - #{I18n.l observation.created_at}:", style: :bold
          pdf.text observation.text
        end
      end
    end

    def attributes
      @position = height - 254

      os_type_attributes = service_order.os_type_attributes

      # Desenhar as linhas
      lines_for_text_box = 4
      text_boxes = os_type_attributes.select(&:text?)

      # Cada atributo de texto ou lista terá uma linha e os de caixa de texto a quantidade definida acima
      # Total de atributos que nao sejam caixa de texto + total de caixas de texto multiplicada pela qtd de linhas
      lines = os_type_attributes.size - text_boxes.size + (text_boxes.size * lines_for_text_box)

      # Divide por dois porque vai desenhar duas linhas por vez, em cima e em baixo
      qtt = lines == 0 ? 6 : (lines / 2).to_i
      qtt += 1 if qtt * 2 < lines

      qtt.times do
        @position -= 30
        pdf.bounding_box([0, position], width: width, height: 15) do
          pdf.stroke do
            pdf.line pdf.bounds.top_left, pdf.bounds.top_right
            pdf.line pdf.bounds.bottom_left, pdf.bounds.bottom_right
          end
        end
      end

      position = height - 280
      row_height = 15

      os_type_attributes.each do |os_type_attribute|
        next if position <= 100

        attr = service_order.attribute_value(os_type_attribute.id)

        case os_type_attribute.data_type
        when AttributeDataType::SELECT then
          attributes = "<b>#{os_type_attribute.name}:</b> "

          os_type_attribute.os_type_attribute_items.each do |i|
            attributes << "#{i.item} #{attr && attr.value == i.item ? checked_box : uncked_box}"
          end

          pdf.bounding_box([5,position + 9], width: width - 10, height: 12) do
            pdf.text attributes, inline_format: true
          end
        when AttributeDataType::STRING then
          pdf.bounding_box([5, position + 6], width: width - 10, height: font_size) do
            pdf.text "<b>#{os_type_attribute.name}:</b> #{attr.value if attr}",
              inline_format: true
          end
        when AttributeDataType::TEXT then
          # 4 linhas pra escrever, cada linha é da altura de 15 pontos
          # Subtrai uma linha porque uma sempre é contada no final do método
          pdf.bounding_box([5, position + 6], width: width - 8, height: (lines_for_text_box * row_height)-5) do
            pdf.text "<b>#{os_type_attribute.name}:</b> #{attr.value if attr}",
              inline_format: true, leading: 5.5
          end

          position -= row_height * (lines_for_text_box - 1)
        end

        position -= row_height
      end
    end

    def products
      return if service_order.service_order_products.blank?

      data = [%w(Código Produto Qtd Valor MAC Tipo)]

      service_order.service_order_products.each do |service_order_product|
        product = service_order_product.product

        data << [
          product.id,
          product.name,
          product.quantity,
          number_to_currency(product.value),
          product.mac,
          service_order_product.classification_humanize
        ]
      end

      pdf.table(
        data,
        header: true,
        column_widths: [50, 215, 50, 60, 100, 50],
        row_colors: row_colors
      ) do
        row(0).font_style = :bold
      end
    end

    def bottom
      if position <= 40
        pdf.start_new_page

        @position = 330

        signature

        pdf.stroke_bounds
      else
        signature
      end

      pdf.number_pages "OS: #{service_order.id} - página <page> de <total>", at: [width - 110, -4], width: 110
    end

    def signature
      super('Assinatura do Cliente', 88, 'Assinatura do Funcionário', 365) do
        if service_order.closing_employee && service_order.finalization_date
          pdf.draw_text "Data de Finalização: #{I18n.l service_order.finalization_date}", at: [10, 43]
          pdf.draw_text "#{service_order.closing_employee.name}", at: [160, 43]
        end
      end
    end
  end
end
