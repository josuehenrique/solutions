module Pdf
  class ProductRequisition < Pdf::Manager
    def initialize(product_requisition)
      @product_requisition = product_requisition
      @business_unit = product_requisition.business_unit

      @klass = ::ProductRequisition
      @width = 525
      @height = 775
      @image = business_unit.logo.path

      super
    end

    def render
      header

      title(klass.model_name.human, 80)

      body_left(102)
      body_right(102)

      line(150)
      reason(165)

      title('Produtos Solicitados', 200, position: 210, with_lines: false)

      pdf.move_up(50)

      pdf.stroke_bounds

      products

      signature('Assinatura do Responsável pelo Estoque', 55, 'Assinatura do Requisitante', 355)

      pdf.stroke_bounds
      pdf.render
    end

    private

    attr_reader :width, :height, :image, :klass, :product_requisition, :business_unit

    def body_left(location)
      pdf.bounding_box([4, height - location], width: (width / 2) -4, height: 102) do
        [:id, :employee, :stock].each do |attr|
          pdf.text "#{klass.human_attribute_name attr}: #{product_requisition.send(attr)}"
        end
        pdf.text "#{klass.human_attribute_name :destiny}: #{ActionView::Base.full_sanitizer.sanitize(product_requisition.destiny.to_s)}"
        pdf.text "#{klass.human_attribute_name :created_at}: #{I18n.l product_requisition.created_at}"
      end
    end

    def body_right(location)
      pdf.bounding_box([(width / 2) + 2, height - location], width: (width / 2) -4, height: 102) do
        pdf.text "#{klass.human_attribute_name :situation}: #{product_requisition.situation_humanize}"
        case product_requisition.situation
        when RequisitionSituation::CANCELED then
          pdf.text "#{klass.human_attribute_name :cancelled_by}: #{product_requisition.cancelled_by}"
          pdf.text "#{klass.human_attribute_name :cancellation_date}: #{I18n.l product_requisition.cancellation_date}"
          pdf.text "#{klass.human_attribute_name :cancellation_reason}: #{product_requisition.cancellation_reason}"
        when RequisitionSituation::DISAPPROVED then
          pdf.text "#{klass.human_attribute_name :disapproved_by}: #{product_requisition.disapproved_by}"
          pdf.text "#{klass.human_attribute_name :disapproval_date}: #{I18n.l product_requisition.disapproval_date}"
          pdf.text "#{klass.human_attribute_name :disapproval_reason}: #{product_requisition.disapproval_reason}"
        when RequisitionSituation::APPROVED then
          pdf.text "#{klass.human_attribute_name :approved_by}: #{product_requisition.approved_by}"
          pdf.text "#{klass.human_attribute_name :approval_date}: #{I18n.l product_requisition.approval_date}"
        end
      end
    end

    def reason(location)
      pdf.draw_text "#{klass.human_attribute_name(:reason)}:", at: [5, height - location], style: :bold, size: 9
      pdf.bounding_box([8, height - location - 5], width: width, height: 102) do
        pdf.text product_requisition.reason
      end
    end

    def products
      data = [%w(Qtd Produto)]

      if product_requisition.service?
        data << [
          1,
          product_requisition.external_service.to_s
        ]
      else
        product_requisition.product_requisition_items.each do |product_requisition_item|
          data << [
            product_requisition_item.quantity,
            product_requisition_item.name
          ]
        end
      end

      pdf.table(
        data,
        header: true,
        column_widths: [30, 495.28],
        row_colors: row_colors
      ) do
        row(0).font_style = :bold
      end
    end

    def title(message, location, options = {})
      position = options.delete(:position) { 205 }
      with_lines = options.delete(:with_lines) { true }

      line(location) if with_lines

      location += 12

      pdf.draw_text message, at: [position, height - location], style: :bold, size: 10

      location += 5

      line(location) if with_lines
    end
  end
end
