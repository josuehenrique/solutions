module Pdf
  class Manager
    include ActionView::Helpers::NumberHelper

    def initialize(*)
      @margin = 35
      @pdf = Prawn::Document.new(
        page_size: 'A4',
        margin: margin,
      )

      # Configurations
      @font_size = 8
      @pdf.font 'Helvetica', size: font_size
    end

    def self.render(*attr)
      new(*attr).render
    end

    private

    attr_reader :margin, :pdf, :font_size

    # Caebeçalho padrão
    def header(position = 90, column = (height - 18), distance = 12)
      set_image(pdf, image, 80, 65, 5, 8, :center, :middle)

      pdf.draw_text business_unit.name.upcase, at: [position, column], style: :bold

      pdf.draw_text "CNPJ: #{Mask.cnpj(business_unit.cnpj)}", at: [position, (column - distance)]

      pdf.draw_text "#{business_unit.street} #{business_unit.number} - #{business_unit.district}",
        at: [position, (column - distance * 2)]

      pdf.draw_text "#{Mask.zipcode(business_unit.zipcode)} - #{business_unit.city}",
        at: [position, (column - distance * 3)]

      pdf.draw_text "Telefone: #{Mask.phone(business_unit.phone_number)}",
        at: [position, (column - distance * 4)]

      pdf.draw_text "Data: #{I18n.l Date.current}", at: [460, column]
      pdf.draw_text "Hora: #{I18n.l Time.current, format: :hour}", at: [460, column - distance]
    end

    # Assinatura no fim da página (2 assinaturas)
    def signature(text1, position1, text2, position2)
      pdf.bounding_box([0, 40], width: width, height: 40) do
        pdf.stroke do
          pdf.line pdf.bounds.top_left, pdf.bounds.top_right
          pdf.line pdf.bounds.bottom_left, pdf.bounds.bottom_right
          pdf.line(10, 13, 240, 13)
          pdf.line(290, 13, 510, 13)
        end
      end

      yield if block_given?

      pdf.draw_text text1, at: [position1, 5]
      pdf.draw_text text2, at: [position2, 5]
    end

    def line(location)
      pdf.line [0, height - location], [width, height - location]
    end

    def dejavu_sans
      "<font name='#{Rails.root}/app/assets/font/DejaVuSans.ttf', size='12'>#{yield}</font>   "
    end

    def uncked_box # => "☐"
      dejavu_sans { "\xE2\x98\x90" }
    end

    def checked_box # => "☒"
      dejavu_sans { "\xE2\x98\x92" }
    end

    def row_colors
      %w(FFFFFF E1E1E1)
    end

    # Posiciona uma imagem em uma área de tamanho e posição especificadas
    # Se a imagem for maior que a área ela será reduzida proporcionalmente de forma a caber dentro dela
    # @param [Prawn] pdf Arquivo pdf que está sendo editado, ao qual será inserido a imagem
    # @param [String] image_path Path da imagem (jpg ou png)
    # @param [Integer] width_area Largura da área que vai ficar a imagem
    # @param [Integer] height_area Altura da área que vai ficar a imagem
    # @param [Integer] to_left_area Distância da área à margem esquerda da página
    # @param [Integer] to_top_area Distância da área ao topo da página
    # @param [Symbol] align Posicionamento horizontal da imagem na área (:left, :center, :right)
    # @param [Symbol] valign Posicionamento vertical da imagem na área (:top, :middle, :bottom)
    def set_image(pdf, image_path, width_area, height_area, to_left_area, to_top_area, align, valign)
      if File.exist?(image_path)
        dimensions = Paperclip::Geometry.from_file(image_path)
        width_img = dimensions.width
        height_img = dimensions.height

        proportion = ((100 * width_area / width_img) / 100).round(2)
        use_proportion = ((width_img > width_area) || (height_img > height_area)) ? true : false
        new_width = use_proportion ? width_img * proportion : width_img
        new_height = use_proportion ? height_img * proportion : height_img

        to_left_img = to_left_area if align == :left
        to_left_img = (((width_area / 2) - (new_width / 2)) + to_left_area) if align == :center
        to_left_img = ((to_left_area + width_area) - new_width) if align == :right

        to_top_img = to_top_area if valign == :top
        to_top_img = (((height_area / 2) - (new_height / 2)) + to_top_area) if valign == :middle
        to_top_img = ((to_top_area + height_area) - new_height) if valign == :bottom

        if use_proportion
          pdf.image image_path, fit: [width_area, height_area],
            position: to_left_img, vposition: to_top_img
        else
          pdf.image image_path, position: to_left_img, vposition: to_top_img
        end
      end
    end
  end
end
