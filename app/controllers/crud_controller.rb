class CrudController < ApplicationController
  #before_filter :authorize_resource!

  inherit_resources

  respond_to :js, :json

  has_scope :page, default: 1, only: [:index, :modal], unless: :disable_pagination?
  has_scope :per_page, default: 10, only: [:index, :modal], unless: :disable_pagination?

  def index(options={}, &block)
    params[:page] = 'all' unless params[:select_id].blank?
    params[:search] ||= {}
    build_resource
  end

  def create
    create! do |success, failure|
      success.html { redirect_to resource_path(search: params[:search]) }
      failure.html { render :new }
    end
  end

  def update
    update! { edit_resource_path }
  end

  def destroy
    destroy! do |_, failure|
      failure.html { redirect_to collection_path }
    end
  end

  def activate
    activate!
  end

  def inactivate
    inactivate!
  end

  def modal
    build_resource
    render partial: 'open_modal'
  end

  protected

  def inactivate!(redirect_params = {}, &block)
    if resource.respond_to?(:active)
      resource.active = false
    else
      resource.active = false
    end

    if resource.save
      block.call if block_given?
      redirect_to collection_path(redirect_params), notice: t('messages.inactivated')
    else
      redirect_to collection_path(redirect_params), alert: t('messages.cant_be_inactivated')
    end
  end

  def activate!(redirect_params = {}, &block)
    if resource.respond_to?(:active)
      resource.active = true
    else
      resource.active = true
    end

    if resource.save
      block.call if block_given?
      redirect_to collection_path(redirect_params), notice: t('messages.activated')
    else
      redirect_to collection_path(redirect_params), alert: t('messages.cant_be_activated')
    end
  end

  def smart_resource_path
    path = nil
    if respond_to? :show
      path = resource_path rescue nil
    end
    path ||= smart_collection_path
  end

  helper_method :smart_resource_path

  def smart_collection_path
    path = nil
    if respond_to? :index
      path ||= collection_path(search: params[:search], page: params[:page]) rescue nil
    end
    if respond_to? :parent
      path ||= parent_path(search: params[:search], page: params[:page]) rescue nil
    end
    path ||= root_path rescue nil
  end

  helper_method :smart_collection_path

  # Build resource using I18n::Alchemy
  def build_resource
    get_resource_ivar || set_resource_ivar(effectively_build_resource)
  end

  # Effectively build resource using I18n::Alchemy
  def effectively_build_resource
    end_of_association_chain.send(method_for_build).tap do |object|
      object.localized.assign_attributes(*resource_params)
    end
  end

  # Update resource using I18n::Alchemy
  def update_resource(object, attributes)
    object.localized.update_attributes(*attributes)
  end

  # Get collection using ordered scope
  def collection
    if params[:search].blank?
      get_collection_ivar || set_collection_ivar(collection_records.ordered)
    else
      searcher
    end
  end

  def collection_records
    if resource_class.respond_to?(:active_filter)
      apply_scopes end_of_association_chain.active_filter(params[:active] || true)
    else
      apply_scopes end_of_association_chain
    end
  end

  def search_params
    params[:search]
  end

  def searcher
    apply_scopes resource_class.searcher_class.search(
      resource.class,
      search_params.merge(
        page: params[:page],
        records: collection_records,
        current_user: current_user
      )
    )
  end

  def disable_pagination?
    params[:page] == 'all'
  end
end
