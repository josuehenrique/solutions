class ApplicationController < ActionController::Base
  protect_from_forgery

  #before_filter :authenticate_user!

  include Notifications

  rescue_from CanCan::Unauthorized, with: lambda { render_error 401 }
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found if Rails.env.production?

  def module_name
    url_for(only_path: true).split('/')[1]
  end

  def render_error(status = 404, exception = nil)
    if request.xhr? && status == 401
      render :js => "alert('Acesso negado!')"
    else
      respond_to do |format|
        format.html { render file: "#{Rails.root}/public/#{status}", status: status }
        format.all { render nothing: true, status: status }
      end
    end
  end

  def show_address
    @values = Zipcode.request_address(params[:zipcode])
    render partial: 'crud/show_address'
  end

  protected

  def open_modal(view = params[:action])
    render partial: 'crud/open_modal', locals: { view: view }
  end

  def errors(object)
    list = object.errors.full_messages
    return nil if list.empty?
    list.join('<br>')
  end

  def authorize_resource!
    authorize! action_name, controller_name
  end

  def after_sign_out_path_for(resource)
    new_user_session_path
  end

  def record_not_found
    redirect_to(collection_path, alert: (t 'messages.record_not_found'))
  end
end
