class GeneralController < CrudController
  defaults resource_class: @klass

  has_scope :page, default: 1, only: [:index]
  has_scope :per_page, default: 10, only: [:index]

  def index
    session[:module] ||= 'crm'

    load_records
  end

  def show
    open_modal
  end

  def create
    create! { redirect_index }
  end

  def update
    update! { redirect_index }
  end

  def destroy
    destroy! { redirect_index }
  end

  private

  def redirect_index
    collection_path(
      object: resource.respond_to?(:related_type) ? resource.related_type : resource.owner_type,
      object_id: resource.respond_to?(:related_id) ? resource.related_id : resource.owner_id,
      module: params[:module]
    )
  end

  def load_records
    @klass = controller_name.singularize.camelize.constantize
    @object = params[:object].constantize.find(params[:object_id])
    instance_variable_set("@#{params[:object].underscore}", @object)
    @records = apply_scopes @object.send(controller_name)
  end
end
