class TeamsController < CrudController
  def new_team
    build_resource

    if resource.students.blank?
      resource.students.build(kind: StudentKind::BUSINESS_ANALYST)
      resource.students.build(kind: StudentKind::DESIGNER)
      resource.students.build(kind: StudentKind::DEVELOPER_1)
      resource.students.build(kind: StudentKind::DEVELOPER_2)
      resource.students.build(kind: StudentKind::DEVELOPER_3)
    end
  end

  def show
    if params[:real_id] == 'Hxs3GzEHe_ttdoD522222222222222222222DB6'
      super
    else
      redirect_to root_path, alert: 'Acesso indevido!!!'
    end
  end

  def create
    create! do |success, failure|
      success.html { redirect_to resource_path(
                                   search: params[:search], real_id: params[:team][:token]) }
      failure.html { render :new }
    end
  end

  def download_regulations
    send_file "#{Rails.root}/public/docs/regulamento_business_academy.pdf",
              type: "application/pdf"
  end

  private

  def create_resource(object)
    if super
      NewTeamMailer.send_subscribe_mail object
    else
      kinds = object.students.collect(&:kind)
      object.students.build(kind: StudentKind::DEVELOPER_2) unless kinds.include? StudentKind::DEVELOPER_2
      object.students.build(kind: StudentKind::DEVELOPER_3) unless kinds.include? StudentKind::DEVELOPER_3
    end
  end
end
