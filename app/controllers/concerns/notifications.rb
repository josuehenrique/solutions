module Notifications
  extend ActiveSupport::Concern

  included do
    before_filter :load_notifications
  end

  def mark_notification_as_seen
    current_user.notifications.by_id(@notifications.map(&:id)).update_all(seen: true)
    not_seen_notifications
    render 'layouts/mark_notification_as_seen'
  end

  protected

  def load_notifications
    return unless current_user

    @notifications = current_user.notifications.ordered.limit(5)
    not_seen_notifications
  end

  def not_seen_notifications
    @not_seen_notifications = current_user.notifications.not_seen.size
  end
end
