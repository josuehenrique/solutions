module Concerns
  module DefaultMailer
    extend ActiveSupport::Concern

    included do
      default from: ['project.business.academy@gmail.com']
      before_action :set_layout_mail
    end

    private

    def dispatch(options = {})
      return true unless Rails.env.production?
      return true if @to.blank?

      options.merge!({ to: @to,bcc: @bcc, subject: @subject })

      mail(options) do |format|
        format.html { render layout: "/layouts/#{@layout}" }
      end.deliver
    end

    def set_layout_mail
      @layout = 'default_mail'
    end
  end
end
