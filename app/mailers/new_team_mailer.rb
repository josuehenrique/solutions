class NewTeamMailer < ActionMailer::Base
  include Concerns::DefaultMailer

  def send_subscribe_mail(team)
    @team = team
    @to = team.students.collect(&:email)
    @subject = "Confirmação de inscrição da Equipe: #{team.name}"
    @logo = "#{Rails.root}/app/assets/images/logo.png"
    @bcc = ['viviane.batista@unievangelica.edu.br','rosana.souza@unievangelica.edu.br']

    attachments['logo.png'] = File.read(@logo)

    dispatch
  end
end
