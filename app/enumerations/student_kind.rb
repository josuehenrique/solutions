class StudentKind < EnumerateIt::Base
  associate_values business_analyst: 'BSA', designer: 'DES',
                   developer_1: 'DV1', developer_2: 'DV2',
                   developer_3: 'DV3'

  def self.required
    [
      StudentKind::BUSINESS_ANALYST,
      StudentKind::DESIGNER,
      StudentKind::DEVELOPER_1
    ]
  end
end
