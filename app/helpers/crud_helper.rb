module CrudHelper
  include AttributeHelper
  include LinkHelper
  include FilterHelper

  def model_file
    resource_class.model_name.to_s.downcase
  end

  def plural(klass = resource_class)
    klass.model_name.human(count: 'many')
  end

  def singular(klass = resource_class)
    klass.model_name.human
  end

  def paginate(records = collection, options = {})
    will_paginate records, options if records.respond_to?(:total_pages)
  end

  def remote_paginate(records = collection)
    if records.respond_to?(:total_pages)
      will_paginate records, renderer: RemotePaginator::LinkRenderer
    end
  end

  def info_box(title, options = {})
    no_border = options.delete(:no_border)
    input_class = options.delete(:input_class)
    badge_count = options.delete(:badge_count)
    collapse = options.delete(:collapse)
    collapsed = options.delete(:collapsed)
    icon = "<i class='#{options.delete(:class_icon)}'></i>".html_safe if options[:class_icon]

    if collapse
      unless collapsed
        collapse_html = "<div class='widget-toolbar'><a data-action='collapse' href='#'><i class='1 icon-chevron-up bigger-125'></i></a></div>".html_safe
      else
        collapse_html = "<div class='widget-toolbar'><a data-action='collapse' href='#'><i class='1 icon-chevron-down bigger-125'></i></a></div>".html_safe
      end
    end

    content_tag :div, class: "control-group #{input_class}" do
      content_tag :div, class: "widget-box #{'collapsed' if collapsed} #{'transparent' if no_border}" do
        lighter = (content_tag :h4, class: 'lighter' do
          if icon
            icon + title
          else
            title
          end
        end)
        badge_count = (content_tag :div, class: "widget-toolbar #{'no-border' if no_border}" do
          (content_tag :span, class: 'badge' do
            badge_count.to_s
          end)
        end)
        header = (content_tag :div, class: 'widget-header widget-header-blue wi1dget-header-large' do
          lighter + collapse_html + badge_count
        end)

        body = (content_tag :div, class: 'widget-body' do
          content_tag :div, class: 'widget-main padding-12' do
            yield
          end
        end)

        header + body
      end
    end
  end

  def pill
    content = (content_tag :div, class: 'col-sm-12' do
      content_tag :ul, class: 'nav nav-pills pill-top' do
        yield
      end
    end)

    content + '<br><br><br>'.html_safe
  end

  def list_table
    content_tag :div, class: 'col-xs-12' do
      content_tag :div, class: 'table-responsive' do
        content_tag :div, class: 'dataTables_wrapper' do
          content_tag :div, class: 'row' do
            yield
          end
        end
      end
    end
  end

  def selectable_boxes
    content_tag :div, class: 'col-xs-6' do
      content_tag :div, class: 'pre-scrollable' do
        content_tag :table, class: 'table table-striped table-bordered table-hover' do
          yield
        end
      end
    end
  end

  def extract_params
    params.dup.to_h.delete_if { |k| %w(action controller search utf8).include?(k) }
  end

  def alert(message, type = :info)
    "<div class='alert alert-block alert-#{type.to_s}'><p>#{message}</p></div>".html_safe
  end

  def nested_box(header, link_to_remove)
    content_tag :div, class: 'widget-box light-border' do
      header = (content_tag :div, class: 'widget-header widget-header-flat widget-header-small' do
        link_to_remove = "<h5 class='smaller'>#{link_to_remove}</h5>"

        if header.blank?
          link_to_remove.html_safe
        else
          (link_to_remove + ' |' + header).html_safe
        end
      end)

      body = (content_tag :div, class: 'widget-body' do
        content_tag :div, class: 'widget-main padding-16' do
          yield
        end
      end)
      raw(header + body)
    end
  end

  def grouped_collection(label_method, records)
    list = []

    records.group_by(&label_method).each do |label, items|
      list << [label.try(:name) || label, items.map { |i| [i.name, i.id] }]
    end

    list
  end
end
