module ApplicationHelper
  include MenuHelper

  def simple_form_for(object, *args, &block)
    options = args.extract_options!
    options[:builder] ||= Base::FormBuilder

    super(object, *(args << options), &block)
  end

  def simple_form_css_class(record, options)
    if record.is_a?(Symbol)
      super
    else
      [super, dom_class(record)]
    end
  end

  def submenu(controller, modulu, options = {})
    @name = options.delete(:name) || (t "controllers.#{controller}")

    @path = "#{modulu}_#{controller}_path"

    render 'layouts/submenu', {controller: controller}
  end

  def area_field(value)
    "#{number_with_precision(value)} m2"
  end

  def smart_report_path
    url_for controller: controller_name, action: :show, id: 'report', only_path: true
  end

  def controller_asset?
    Rails.application.assets.find_asset controller_name
  end

  # Apply I18n::Alchemy on a collection of active record objects.
  #
  # Usage:
  #
  #   localized(products) do |product|
  #     product.price #=> "1,50"
  #   end
  #
  #   products = localized(products)
  #   products.first.price #=> "1,50"
  def localized(collection_object)
    if block_given?
      collection_object.each { |object| yield(object.localized) }
    else
      collection_object.map { |object| object.localized }
    end
  end

  def options_for_input(object_setting)
    options = {}
    if object_setting
      options.merge!(label: object_setting.name)
      options.merge!(collection: object_setting.options.map(&:name)) if object_setting.collection?
      options.merge!(as: :boolean) if object_setting.boolean?
      options.merge!(as: :date) if object_setting.date?
      options.merge!(as: :datetime) if object_setting.datetime?
      options[:input_html] ||= {}
      options[:input_html]['data-attribute-name'] = object_setting.name.parameterize.to_s
      if object_setting.respond_to?(:dependency) && object_setting.dependency.present?
        options[:input_html]['data-dependency'] = object_setting.dependency.name.parameterize.to_s
        options[:input_html][:disabled] = :disabled
      end
    end
    options
  end

  def find_input_for(value, setting)
    Base::FindInput.new(value, setting).find
  end

  def mustache(name, &block)
    content_tag(:script, id: name, type: 'text/x-mustache') do
      content = capture(&block)
      content = content.gsub("</script>", "<\\/script>")
      content.html_safe
    end
  end

  def modal(header)
    header = content_tag :div, class: 'modal-header no-padding' do
      content_tag :div, class: 'table-header' do
        raw("<button class='close' data-dismiss='modal' type='button'>×</button> #{header}")
      end
    end

    body = content_tag :div, class: 'modal-body' do
      content_tag :div, class: 'dataTables_wrapper' do
        content_tag :div, class: 'row-fluid' do
          yield
        end
      end
    end

    header + body
  end

  def notification_url(notification)
    if notification.error
      "/general/notifications/#{notification.id}"
    else
      notification.url || '#'
    end
  end
end
