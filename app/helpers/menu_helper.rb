module MenuHelper
  def menu_id(key)
    path = url_for(only_path: true).split('/')

    return if path.size < 3

    send(key.to_s, path)
  end

  private

  def sub(path)
    submenu = path.delete_at(2)

    if %w(client_attendances attendances aliases archives directories emails
            permits addresses).include? submenu
      %w(Client Employee BusinessUnit Department Post).each do |item|
        submenu = item.underscore.pluralize if params.values.include?(item) ||
          (params.values.include?('ClientPlan') && item == 'Client')
      end
    else
      submenu = case submenu
                when 'cities' then
                  'states'
                when 'business_unit_os_configurations' then
                  'business_units'
                when 'server_service_domains' then
                  'servers'
                when 'client_plans' then
                  'clients'
                when 'products' then
                  'stocks'
                else
                  submenu
                end
    end

    'sub_' + submenu
  end

  def menu(path)
    menu = path.delete_at(1)
    menu = 'enterprise' if (params.values & %w(Employee BusinessUnit Department)).size > 0
    menu = 'crm' if (params.values & %w(Client Directories ClientPlan)).size > 0

    'menu_' + menu
  end
end
