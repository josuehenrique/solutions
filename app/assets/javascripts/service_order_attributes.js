(function () {
  function changeLink() {
    var radiusUserId = $('#os_type_attributes_radius_user_id').val();
    var link = $('#mk_search');
    var href = link.attr('href');

    if (href) {
      link.attr('href', href.replace(/radius_user_id=[^&]+/, 'radius_user_id=' + radiusUserId));
    }
  }

  changeLink();

  $('#os_type_attributes_radius_user_id').change(function () {
    changeLink();
  });
})();
