$(function () {
  var target = $('.wizard-steps li.active').data('target');
  var clientPlansTarget = $('#client_plans');

  if ((target == '#step4') || (clientPlansTarget != '')) {
    $("[id$='payment_form_id']").change(function () {
      var url = $(this).data('url');
      var paymentFormId = $(this).val();

      if (paymentFormId) {
        $.get(url, {payment_form_id: paymentFormId}, 'script');
      }
    });

    $("[id$='accession_plots_quantity']").change(function () {
      var url = $(this).data('url'),
        opportunityId = $("[id$='opportunity_id']").val(),
        expirationDate = $('#client_client_plan_expiration_date_id :selected').text(),
        contractId = $("[id$='contract_id']").val();

      $.get(url, {
        accession_plots_quantity: $(this).val(),
        expiration_date: expirationDate,
        contract_id: contractId,
        opportunity_id: opportunityId
      }, 'script');
    });

    $('#client_plan_business_unit_id').change(function () {
      var url = $(this).data('url');
      $.get(url, { by_business_unit: $(this).val() }, 'script');
      $('#payment_form').removeClass('hide');
    });
  }
});
