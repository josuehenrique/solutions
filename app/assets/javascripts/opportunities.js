(function () {
  var target = $('.wizard-steps li.active').data('target');
  var opportunities_target = $('.nested-fields').data('target');

  if ((target == '#step2') || (opportunities_target == 'opportunities')) {
    // Carrega os contratos, conforme o plano
    function contractsByPlan(el) {
      var id = el.attr('id').replace('plan_id', 'contract_id');

      $.get('/configurations/contracts', {
        by_plan: el.val(),
        select_id: id
      }, null, 'script');
    }

    // Carrega os valores do plano, já aplicando os descontos do contrato
    function totalValue(el, plan_id, contract_id) {
      $.get('/enterprise/plans/' + plan_id + '.json', {contract_id: contract_id}, function (data) {
        el.find('#accession_total').html(data.accession_value_with_discount);
        el.find('#installation_total').html(data.installation_value_with_discount);
        el.find('#monthly_total').html(data.monthly_value_with_discount);
      });
    }

    function loadPlanValues() {
      $("[id$='plan_id']").change(function () {
        contractsByPlan($(this));

        var el = $(this).closest('.row'),
            contractId = el.find("[id$='contract_id']").val(),
            planId = $(this).val();

        if (planId) {
          // Carrega os valores totais do plano
          $.get('/enterprise/plans/' + planId + '.json', function (data) {
            el.find('#accession_value').html(data.accession_value);
            el.find('#installation_value').html(data.installation_value);
            el.find('#monthly_value').html(data.monthly_value);
          });
        }

        if (contractId && planId) {
          totalValue(el, planId, contractId);
        }
      });

      $("[id$='contract_id']").change(function () {
        var el = $(this).closest('.row'),
            contractId = $(this).val(),
            planId = el.find("[id$='plan_id']").val();

        if (contractId && planId) {
          // Carrega a variação do contrato
          $.get('/configurations/contracts/' + contractId + '.json', function (data) {
            el.find('#plan_variation').html(data.plan_variation);
            el.find('#accession_variation').html(data.accession_variation);
            el.find('#installation_variation').html(data.installation_variation);

            var plotsInput = el.find("[id$='adhesion_plots']");
            var opportunitieId = el.find('.id').val();

            if (opportunitieId == '') {
              loadAdhesionSelect(plotsInput, data.accession_plots_qtt, null);
            } else {
              var opportunityUrl = el.find('.id').data('opportunity-url');

              $.get(opportunityUrl, function (opportunitie) {
                loadAdhesionSelect(plotsInput, data.accession_plots_qtt, opportunitie.adhesion_plots);
              });
            }
          });

          totalValue(el, planId, contractId);
        }
      });
    }

    // Carrega o select com a quantidade de parcelas de adesão disponíveis
    function loadAdhesionSelect(plotsInput, adhesionPlots, plotSelected) {
      plotsInput.html('<option>Selecione</option>');
      for (var i = 1; i <= adhesionPlots; i++) {
        if (i == plotSelected) {
          plotsInput.append(new Option(i, i, false, true));
        } else {
          plotsInput.append(new Option(i, i));
        }
      }
    }

    loadPlanValues();

    $(document).ready(function () {
      $("[id$='plan_id']").trigger('change');
      $("[id$='contract_id']").trigger('change');
    });

    $('#step2').on('cocoon:after-insert', function () {
      $("[id$='plan_id']").change(function () {
        contractsByPlan($(this));
      });

      loadPlanValues();
    });
  }
})();
