// Decimal Fields
$(document).on("focus", "input[data-decimal]", function () {
  var input = $(this);

  setPriceFormat(input);
});

function setPriceFormat(field){
  field.priceFormat({
    prefix: false,
    thousandsSeparator: ".",
    centsSeparator: ",",
    centsLimit: field.data("precision"),
    allowNegative: field.data("negative")
  });
}

(function ($) {
  $('input.decimal').each(function( index, value ) {
    setPriceFormat($('#' + value.id));
  });
})(jQuery);

// Numeric Fields
(function ($) {
  var pasteEventName = 'input';

  $.fn.singlemask = function (mask) {
    $(this).on('keydown',function (event) {
      var key = event.keyCode;

      if (key < 16 || (key > 16 && key < 32) || (key > 32 && key < 41)) {
        return;
      }

      return String.fromCharCode(key).match(mask);
    }).on(pasteEventName, function () {
        this.value = $.grep(this.value,function (character) {
          return character.match(mask);
        }).join('');
      });
  }
})(jQuery);

(function ($) {
  $("input[data-numeric]").singlemask(/\d/);
})(jQuery);

// File input
$(document).ready(function () {
  $("input.file").ace_file_input({
    no_file: 'Sem Arquivo ...',
    btn_choose: 'Selecione',
    btn_change: 'Alterar',
    droppable: false,
    onchange: null,
    thumbnail: false
  });
});

// Add span in checkbox
hint = $('label.checkbox').parent().find('.help-inline');
$('label.checkbox').append(hint);

// Modal Url
function reloadModal() {
  $('input[data-modal-url]').unbind('click');

  $('input[data-modal-url]').on('click', function () {
    $(this).parent().find('a').click();
  });

  $('.modal_input_icon_search').unbind('click');

  $('.modal_input_icon_search').on('click', function () {
    $(this).parent().find('a').click();
  });
};

reloadModal();

// Upcase and remove special characters
function clean_format(obj, opt) {
  if (opt) {
    var name_without_special_char = obj.value.replace(/[^a-zA-Z 0-9]+/g, "");
    obj.value = name_without_special_char.toUpperCase();
  } else {
    obj.value = obj.value.toUpperCase();
  }
}
