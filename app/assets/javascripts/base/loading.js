if (environment !== "test") {
  function addLoading() {
    $('body').append('<div class="loading"></div>');
    var spinner = new Spinner().spin();
    $('body').append(spinner.el);
  }

  function removeLoading() {
    $('.spinner, .loading').remove();
  }

  $(document).ajaxStart(function () {
    addLoading();
  });

  $(document).ajaxComplete(function () {
    removeLoading()
  });
}
