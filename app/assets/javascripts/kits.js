function controlKitFields(category) {
  var subCategory = $(".subcategory"),
      productType = $(".product_type");

  if (category == 'true') {
    productType.addClass('hidden');
    subCategory.removeClass('hidden');
  } else {
    productType.removeClass('hidden');
    subCategory.addClass('hidden');
  }
}

$("[id^='kit_by_category']").change(function () {
  controlKitFields($(this).val());
});

$(function() {
  var byCategory = $("[id^='kit_by_category']:checked");

  controlKitFields(byCategory.val());

  $('body').on('cocoon:after-insert', function (e, insertedItem) {
    var byCategory = $("[id^='kit_by_category']:checked");

    controlKitFields(byCategory.val());
    reloadModal();
  });
});
