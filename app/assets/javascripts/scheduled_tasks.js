(function() {
  $('#scheduled_task_classification').change(function () {
    var url = $(this).data('url'),
        target = 'scheduled_task_client_plan_situation_id',
        classification = $(this).val();

    if (classification == 'S') {
      $.get(url, {suspended_service_and_bill: true, select_id: target}, 'script');
    } else if (classification == 'R') {
      $.get(url, {active_plan: true, select_id: target}, 'script');
    }
  });
})();
