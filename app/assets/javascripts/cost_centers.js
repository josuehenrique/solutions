function analytic() {
  return $('#cost_center_classification_a').is(':checked')
}

function changeKindFields() {
  var kind = $("[name='cost_center[kind]']");

  if (analytic()) {
    kind.attr('disabled', false);
  } else {
    kind.attr('disabled', true).attr('checked', false);;
  }
}

changeKindFields()

$("[name='cost_center[classification]']").change(function() {
  changeKindFields()
});
