$("[id='attendance_client_id']:hidden").change(function () {
  var link = $('#attendance_client_plan_id_link'),
      clientPlan = $('#attendance_client_plan_id'),
      clientPlanHidden = $("[id='attendance_client_plan_id']:hidden"),
      clientId = $(this).val(),
      url = '/crm/clients/' +
        clientId +
        '/client_plans/modal?input_id=attendance_client_plan_id'

  link.attr('href', url);
  clientPlan.attr('disabled', false);
  clientPlan.val('');
  clientPlanHidden.val('');
});

$(function() {
  var inputId = $("#attendance_client_id"),
      inputIdHidden = $("[id='attendance_client_id']:hidden"),
      clientId = inputId.data('client-id'),
      client = inputId.data('client');

  if (clientId != undefined) {
    inputId.val(client);
    inputIdHidden.val(clientId);
    inputIdHidden.trigger('change');
  }

  $('.add_fields').click();
  $('.remove_fields').hide();
});
