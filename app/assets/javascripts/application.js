// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//= require base/jquery.cookie
//= require fuelux.wizard
//= require cocoon
//= require cocoon_methods

//= require ace/bootstrap.js
//= require ace/ace-elements.js
//= require ace/ace-extra.js
//= require ace/ace.js
//= require ace/bootbox.js
//= require base/jquery.price_format.1.8.js
//= require base/jquery.maskedinput.js
//= require base/rails.js
//= require date-time/bootstrap-datepicker.js
//= require date-time/moment.min.js
//= require date-time/bootstrap-datetimepicker.min.js
//= require date-time/bootstrap-datetimepicker.pt-BR.js
//= require date-time/daterangepicker.js
//= require base/fields.js
//= require base/jquery.slimscroll.min.js
//= require base/jquery-mustache.js
//= require fuelux/fuelux.tree.js
//= require base/simple_form.validation
//= require base/spin
//= require base/loading

//= require phone-mask.js
//= require scripts.js

$('#notifications').on('click', function() {
  $.get('/mark_notification_as_seen');
});

function control_el_display(click_el, related_el) {
  click_el.click(function() {
    if (related_el.is(":visible")) {
      related_el.hide();
    } else {
      related_el.show();
    }
  });
}

function hide_link(filter) {
  $('#' + filter).show();
  $('#link_' + filter).hide();
}

// Retorna o atributo que está na option do select
function selectedAttr(select, attr) {
  return $('#' + select.attr('id') + ' option:selected').attr(attr)
}

// Busca o elemento mais próximo dentro do item (cocoon)
function item(element, id) {
  var div = element.closest('div.widget-main');

  return div.find("[id$='" + id + "']")
}

function closeModal() {
  $('#modal-window').modal('hide');
}

// Limpa a lista de itens clicados da árvore
$('.sidebar span').click(function (){
  $.removeCookie('tree_items')
});
