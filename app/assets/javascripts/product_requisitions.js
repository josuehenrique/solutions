$(function() {
  function controlInputs(destinyId, tab, html) {
    var inputId = 'input_id=product_requisition_destiny_id',
        link = $('#product_requisition_destiny_id_link');

    // Obtém a url que está no data-xxx-url do input para alterar no link
    var url = destinyId.attr('data-' + tab.attr('data-url') + '-url');

    if (url.indexOf('?') == -1) {
      url = url + '?' + inputId;
    } else {
      url = url + '&' + inputId;
    }

    link.attr('href', url);

    // Altera o label do campo para o nome da tab
    $("label[for='product_requisition_destiny_id']").html('<abbr title="Obrigatório">*</abbr>' + html)
  }

  function serviceControl(html) {
    var general = $('#general'),
        service = $('#service'),
        btn = $('.form-actions .btn-info');

    if (html == 'Serviço') {
      general.hide();
      service.removeClass('hidden');
      btn.html('Salvar');
    } else {
      general.show();
      service.addClass('hidden');
      if (window.current_action == 'edit') {
        btn.html('Salvar');
      } else {
        btn.html('Salvar e Selecionar produtos');
      }
    }
  }

  $('#product_requisition_tab a').on("click", function(){
    var tab = $(this),
        destinyId = $('#product_requisition_destiny_id'),
        destinyIdHidden = $("[id='product_requisition_destiny_id']:hidden"),
        destinyTypeHidden = $("[id='product_requisition_destiny_type']:hidden"),
        html = $(this).html();

    destinyIdHidden.val('');
    destinyTypeHidden.val('');
    destinyId.val('');

    serviceControl(html);

    controlInputs(destinyId, tab, html);
  });

  var destinyType = $("[id='product_requisition_destiny_type']:hidden").val();

  if (destinyType == '') {
    if ($('#product_requisition_external_service_id').val() != '') {
      $('#ExternalService').click();
    }
  }

  // Se o destino estiver preenchido, escolhe a tab correta e muda o campo
  if (destinyType) {
    var tab = $('#' + destinyType),
        destinyId = $('#product_requisition_destiny_id');

    controlInputs(destinyId, tab, tab.html());

    // Define active para a tab correta
    $('.tabbable li').removeClass('active');
    tab.parent().addClass('active');
  }
});
