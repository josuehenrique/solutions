//= require opportunities
//= require client_plans

(function() {
  var target = $('.wizard-steps li.active').data('target')

  if (target == '#step4') {
    $("[id$='opportunity_id']").change(function () {
      var url = $(this).data('url'),
          opportunityId = $(this).val();

      if (opportunityId) {
        $.get(url, {opportunity_id: opportunityId}, 'script');
      }
    });

    $("[id$='contract_id']").change(function () {
      var url = $(this).data('url'),
          opportunityId = $("[id$='opportunity_id']").val();

      $.get(url, {contract_id: $(this).val(), opportunity_id: opportunityId}, 'script');
    });
  }
})();
