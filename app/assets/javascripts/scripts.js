jQuery.ajaxSetup({
  'beforeSend': function(xhr) { xhr.setRequestHeader("Accept", "text/javascript") }
});

function checkboxes_all(sender, targets){
  $(sender).click(function() {
    if(this.checked) {
      $(targets).each(function() {
        this.checked = true;
      });
    }
    else {
      $(targets).each(function() {
        this.checked = false;
      });
    }
  });
}

$(document).ready(function () {
  // User suggestion
  $( ".user_link" ).on( "click", function() {
    $(this).closest('div').find('input').val($(this).text().trim());
    return false
  });

  // Link to new object
  $.each($('select'), function( index, value ) {
    var el = $('#'+value.id),
      link = el.attr('link_to_new'),
      location = el.parent();

    if (link) {
      location.append("" +
        "<a href="+link+" id='link_"+value.id+"' data-remote='true' data-target='modal-window' data-toggle='modal'>" +
        "<i class='icon-expand-alt bigger-200'></i></a>");
    }
  });
});

// Slim scrool
$(function() {
  $('.pre-scrollable').slimScroll({
    height: '340px'
  });
});

// Pills
$('.nav.nav-pills a').click(function() {
  var liId = $(this).parent().attr('id');
  $.cookie('pill', liId);
});

// Possibilita marcar a 'pill' atual conforme a ação na URL
if ($.cookie('pill') == 'null') {
  var current_action = location.pathname.split('/').pop();
  $.cookie('pill', current_action);
}

// Remove all active pills and set the current active
var el = $('#'+$.cookie('pill'));
el.parent().children().removeClass('active');
el.addClass('active');
$.cookie('pill', null);

// Remove selected pill when click sidebar menu
$('#sidebar li [id^=sub]').click(function() {
  $.removeCookie('pill');
});

// Functions TO NOT USE on TEST environment
function setDatePicker() {
  $('.date-picker').datepicker({
    format: 'dd/mm/yyyy',
    language: 'pt-BR',
    todayHighlight: true
  });
}

function setDateTimePicker() {
  $('input.datetime').datetimepicker({
    language: 'pt-br'
  })
}

function setDateRangePicker() {
  $('.date-range-picker').daterangepicker({
    format: 'DD/MM/YYYY',
    language: 'pt-BR',
    locale: {
      applyLabel: 'Aplicar',
      cancelLabel: 'Cancelar',
      fromLabel: 'De',
      toLabel: 'Até',
      weekLabel: 'S',
      customRangeLabel: 'Custom Range',
      firstDay: 0
    }
  });
}

if (environment !== "test") {
  $('[data-rel=tooltip]').tooltip();

  setDatePicker();

  setDateTimePicker();

  setDateRangePicker();

  $(document).ready(function () {
    $('input.clean_format').attr('onkeyup', 'clean_format(this, true)');
    $('input.clean_format_without').attr('onkeyup', 'clean_format(this, false)');
  });
}

// Remove mask on submit
$('form').submit(function () {
  $('.mask-zipcode').mask("99999999");
  $('.mask-cnpj').mask("99999999999999");
  $('.mask-cpf').mask("99999999999");

  unmask_phone($('.mask-phone'));

  $('input.decimal').each(function( index, value ) {
    ($('#' + value.id)).unpriceFormat();
  });
});

//Add mask
function addMasks() {
  $('.mask-zipcode').mask("99.999-999");
  $('.mask-cnpj').mask("99.999.999/9999-99");
  $('.mask-cpf').mask("999.999.999-99");
  $('.date-picker').mask("99/99/9999");
  $('input.datetime').mask("99/99/9999 99:99");
  $('.mask-num-bank').mask("999");
  $('.mask-dv-agency').mask("9");
  $('.mask-acronym').mask("aa");
  $('.mask-mac').mask("HH:HH:HH:HH:HH:HH");

  mask_phone($('.mask-phone'));
}

addMasks();
