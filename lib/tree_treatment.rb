class TreeTreatment
  def self.create_structure(*attr)
    new(*attr).create_structure
  end

  def initialize(objects, additional_params = {})
    @objects = objects
    @data = {}
    @pluralized = objects.first.class.model_name.plural unless objects.blank?
    @additional_params = additional_params
  end

  def create_structure
    @objects.roots.each do |root|
      crete_root root

      root.self_and_descendants.each do |object|
        next unless object.parent_id

        create_child object
      end
    end

    @data
  end

  protected

  attr_reader :additional_params

  def crete_root(root)
    add_element(@data, root)
    child_structure(@data[root.id])
  end

  def create_child(object)
    # Find the related node at tree
    obj = @data.deep_find(object.parent_id)

    child_structure(obj)
    add_element(obj['additionalParameters']['children'], object)
  end

  # This structure allow the element to recinherit_resourceseive children
  def child_structure(recipient)
    recipient['additionalParameters'] ||= {'children' => {}} unless recipient.has_key?('additionalParameters')
  end

  def add_element(recipient, object)
    if object.active?
      data = { action: 'inactivate', title: 'Inativar', icon: 'minus', color: 'red' }
    else
      data = { action: 'activate', title: 'Ativar', icon: 'ok', color: 'green' }
    end

    link = "#{object.to_s.upcase}
           <div class='pull-right action-buttons' id='#{object.class.model_name.to_s.underscore}_#{object.id}'>
            <a href='#{new(object)}' class='grey' title='Adicionar'><i class='icon-plus-sign bigger-130'></i></a>
            <a href='#{edit(object)}' class='blue' title='Editar'><i class='icon-pencil bigger-130'></i></a>
            <a href='#{change_situation(object, data[:action])}' data-confirm='Você tem certeza?' class='#{data[:color]}' title='#{data[:title]}'><i class='icon-#{data[:icon]}-sign bigger-130'></i></a>
           </div>"
    recipient[object.id] = {name: link, type: type(object)}
  end

  private

  def type(object)
    object.leaf? ? 'item' : 'folder'
  end

  def add_params_to_link(link)
    additional_params.each do |key, value|
      link += link.include?('?') ? "&#{key}=#{value}" : "?#{key}=#{value}"
    end

    link
  end

  def new(object)
    link = "#{@pluralized}/new?parent_id=#{object.id}"
    add_params_to_link link
  end

  def edit(object)
    add_params_to_link "#{@pluralized}/#{object.id}/edit"
  end

  def change_situation(object, action)
    add_params_to_link "#{@pluralized}/#{object.id}/#{action}"
  end
end
