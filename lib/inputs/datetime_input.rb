module Inputs
  class DatetimeInput < SimpleForm::Inputs::Base
    def input
      @builder.text_field(attribute_name, input_html_options)
    end

    protected

    def input_html_classes
      super.unshift('string datetime')
    end

    def input_html_options
      super.tap do |options|
        options[:size] ||= 14
        options[:data] ||= {}
      end
    end
  end
end
