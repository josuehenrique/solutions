namespace :tree do
  desc "Generate codes for account_plans"
  task planoconta_codes: :environment do
    AccountPlan.find_each do |account_plan|
      account_plan.set_code
    end
  end

  task cost_center_codes: :environment do
    CostCenter.find_each do |cost_center|
      cost_center.set_code
    end
  end
end
