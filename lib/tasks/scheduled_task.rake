namespace :scheduled_task do
  desc 'Responsável por executar diariamente as tarefas programdas'
  task(execute: :environment) do
    # Pode ser passado o parâmetro date, para executar a tarefa de outro dia
    # por padrão, ele executa as do dia atual
    # ex.: ScheduledTaskMaker.start(date: '10/10/2014'.to_date)
    ScheduledTaskMaker.start
  end
end
