namespace :fix_production_database do
  desc 'Ajusta banco de dados de produção'
  task(start: :environment) do
    sql = File.read('db/fix_production.sql')
    statements = sql.split(/;$/)
    statements.pop

    ActiveRecord::Base.transaction do
      statements.each do |statement|
        ActiveRecord::Base.connection.execute(statement)
      end
    end
  end
end
