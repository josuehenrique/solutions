namespace :reorganize_attendance_classifications do
  desc 'As classificações estavam ligadas em filas, foi alterado para setor e para estrutura de árvore'
  task do: :environment do
    attendance_classification_table = AttendanceClassification.arel_table

    ids = {
      1 => 1,  # Financeiro
      16 => 1,
      18 => 1,
      20 => 1,
      21 => 1,
      31 => 1,
      32 => 1,
      47 => 1,
      48 => 1,
      49 => 1,
      50 => 1,
      2 => 2,  # Comercial
      19 => 2,
      25 => 2,
      33 => 2,
      40 => 2,
      41 => 2,
      59 => 2,
      71 => 2,
      17 => 2,
      43 => 2,
      46 => 2,
      52 => 2,
      53 => 2,
      54 => 2,
      55 => 2,
      57 => 2,
      58 => 2,
      61 => 2,
      66 => 2,
      69 => 2,
      3 => 3,  # RH
      15 => 5,  # Suporte
      4 => 5,
      44 => 5,
      62 => 5,
      68 => 5,
      5 => 6,  # Redes
      26 => 7,  # Administração
      27 => 7,
      28 => 7,
      29 => 7,
      30 => 7,
      34 => 7,
      6 => 7,
      37 => 7,
      60 => 7,
      67 => 7,
      74 => 7,
      7 => 8,  # Compras
      8 => 9,  # Controladoria
      22 => 9,
      63 => 9,
      70 => 9,
      73 => 9,
      75 => 9,
      9 => 10, # TI
      39 => 10,
      24 => 11, # Ouvidoria
      10 => 11,
      51 => 11,
      72 => 11,
      11 => 12, # Diretoria
      12 => 14, # NOC
      13 => 14,
      23 => 14,
      35 => 14,
      36 => 14,
      38 => 14,
      42 => 14,
      56 => 14,
      64 => 14,
      65 => 14,
      14 => 16, # Patrimônio
      45 => 17, # Desenvolvimento
    }

    ids.each do |request_queue_id, department_id|
      records = AttendanceClassification.where(request_queue_id: request_queue_id)
      records.update_all(department_id: department_id)
    end


    # Os que sobraram são colocados em algum setor
    records = AttendanceClassification.where(
      attendance_classification_table[:department_id].eq(nil).
        and(attendance_classification_table[:request_queue_id].not_eq(nil))
    )
    records.update_all(department_id: 1)

    # Colocar na árvore
    records = AttendanceClassification.where(
      attendance_classification_table[:department_id].not_eq(nil)
    )

    records.group_by(&:department_id).each do |_, items|
      alleged_problem = items.select { |r| r.kind == AttendanceClassificationLevel::ALLEGED_PROBLEM }
      identified_problem = items.select { |r| r.kind == AttendanceClassificationLevel::IDENTIFIED_PROBLEM }
      solution = items.select { |r| r.kind == AttendanceClassificationLevel::SOLUTION }

      if alleged_problem.any?
        identified_problem.each do |record|
          record.parent_id = alleged_problem.first.id
          record.save
        end

        if identified_problem.any?
          solution.each do |record|
            record.parent_id = identified_problem.first.id
            record.save
          end
        end
      end
    end
  end
end
