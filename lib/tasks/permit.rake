namespace :permit do
  desc "Loading all models and their related controller methods inpermissions table."
  task(create: :environment) do
    Controller.list.each do |controller|
      #only that controller which represents a model
      name = Controller.extract_name(controller)
      # Module
      m_name = name[0]
      # Controller
      c_name = name[1]

      puts "#{m_name}: #{c_name}" unless Rails.env.test?

      write_permission(m_name, c_name, 'access', 'Acesso Total')

      next if action_list(controller).blank?

      action_list(controller).each do |method|
        if method =~ /^([A-Za-z\d*]+)+([\w]*)+([A-Za-z\d*]+)$/ #add_user, add_user_info, Add_user, add_User
          name, cancan_action = eval_cancan_action(method)
          write_permission(m_name, c_name, cancan_action, name)
        end
      end
    end
  end
end

def action_list(controller)
  # new e edit são as mesmas permissões de create e update
  controller.action_list - [:new, :edit]
end

#this method returns the cancan action for the action passed.
def eval_cancan_action(action)
  name = I18n.t("actions.#{action}")

  raise "Ação sem tradução: #{action}" if name.include?('translation missing')

  return name, action.to_s
end

# Check if the permission is present else add a new one.
def write_permission(modulus, controller, cancan_action, name)
  permission = Permit.where(
    modulus: modulus,
    controller: controller,
    action: cancan_action
  ).first

  unless permission
    if I18n.t("controllers.#{controller}").include?('translation missing')
      raise "Controller sem tradução: #{controller}"
    end

    permission = Permit.new
    permission.name = name
    permission.modulus = modulus
    permission.controller = controller
    permission.action = cancan_action
    permission.save!
  end
end
