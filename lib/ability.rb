class Ability
  include CanCan::Ability

  def initialize(user = nil)
    alias_action :modal, to: :read

    return unless user

    if user.administrator?
      can :access, :all
    else
      can :modal, :all
      can :access, :notifications

      set_user_permissions(user)

      aggregated_permissions
    end
  end

  def set_user_permissions(user)
    if user.roles.blank? && user.employee
      user.employee.post.roles.each do |role|
        can role.action.to_sym, role.controller.to_sym
      end
    else
      user.roles.each do |role|
        can role.action.to_sym, role.controller.to_sym
      end
    end
  end

  # Quando o usuário tem permissão para X também tem para Y
  def aggregated_permissions
    if can? :index, :attendance_classifications
      can :show_detailed, :attendance_classifications
    end

    if can? :create, :payment_forms
      can :banks, :payment_forms
    end

    if can? :create, :pools
      can :calculate_network, :pools
    end

    if can? :create, :service_orders
      can :load_service_orders, :service_orders
    end

    if can? :create, :clients
      can :verify_payment_form_billet, :clients
      can :contracts_by_opportunity, :clients
      can :insert_adhesion, :clients
      can :show_plots, :clients
    end
  end
end
