module Helpers
  def self.included(receiver)
    receiver.let!(:current_user) do
      FactoryGirl.cache(:admin)
    end
  end

  def test_image
    "#{Rails.root}/spec/fixtures/images/test.jpg"
  end

  def print
    @file ||= 0
    @file += 1
    page.driver.render(@file.to_s + '.png', full: true)
  end

  def check_active_menu(text)
    expect(page).to have_xpath ".//li[@class='open active']//a[contains(., '#{text}')]"
  end

  def check_active_submenu(text)
    expect(page).to have_xpath ".//li[@class='active']//a[contains(., '#{text}')]"
  end

  def sign_in(user = current_user)
    visit root_path

    fill_in 'user_login', with: user.login
    fill_in 'user_password', with: user.password

    click_button 'Entrar'
  end

  def user_access_object_permission(text, object)
    expect(page).to_not have_content text.upcase
    FactoryGirl.cache(:employee_relation, employee: @user.employee, related: object)
    visit(current_path)
    expect(page).to have_content text.upcase
  end

  def tr_with_text(text)
    within page.find('tr', text: text) do
      yield
    end
  end

  def select_field(locator, opts)
    should_include = opts[:should_not_include].blank?

    options = opts[:should_include] || opts[:should_not_include]
    options = [options] if options.is_a?(String)

    field = page.find(:select, locator)

    within ("select##{field[:id]}") do
      options.each do |option|
        if should_include
          expect(page).to have_content option
        else
          expect(page).to_not have_content option
        end
      end
    end
  end

  def within_search_form
    within 'form.form-search' do
      yield
    end
  end

  def search(locator, keyword)
    within_search_form do
      select locator, from: 'search_attr'
      fill_in 'search_keyword', with: keyword

      click_button 'search'
    end
  end

  def filter(attributes)
    within_search_form do
      attributes.each do |field, selected|
        select selected, from: "search_filter_#{field.to_s}"
      end

      click_button 'search'
    end
  end

  def filter_date(attr, value)
    within_search_form do
      click_link attr

      fill_in attr, with: value

      click_button 'search'
    end
  end

  def within_first_fieldset
    within 'fieldset:first' do
      yield
    end
  end

  def within_last_fieldset
    within 'fieldset:last' do
      yield
    end
  end

  def navigate(path)
    first, second = path.split(/ > /, 2)

    click_link first

    sleep 0.1

    return unless second

    sleep 0.1

    within :xpath, ".//a[contains(., '#{first}')]/following-sibling::ul" do
      navigate second
    end
  end

  def click_record(record)
    begin
      within 'table' do
        page.find('td', text: record).click
      end
    rescue Capybara::ElementNotFound
      raise Capybara::ElementNotFound, "Unable to find text '#{record}' on css 'td'"
    end
  end

  def click_pill(record)
    within '.nav.nav-pills' do
      page.find('a', text: record).click
    end
  end

  def within_tab(locator)
    within '.tabbable' do
      click_link locator

      within '.tab-pane.active' do
        yield
      end
    end
  end

  def fake_select(element_id, val)
    page.execute_script %{$('##{element_id} :selected').val('#{val}');}
  end

  def click_modal_link(locator)
    field = page.find_field(locator)
    page.execute_script %{ $('##{field[:id]}').click() }
    wait_for_ajax
  end

  def fill_modal(locator, options = {})
    within_modal(locator) do
      click_record options.fetch(:with)
    end
  end

  def within_modal(locator)
    click_modal_link(locator)
    within_modal_window do
      yield
    end
  end

  def within_modal_window
    within :xpath, '(//div[@id="modal-content"])[last()]' do
      yield
    end
  end

  def close_modal
    page.execute_script %{ $('.table-header button').click() }
  end

  def wait_for_ajax
    Timeout.timeout(Capybara.default_wait_time) do
      loop until finished_all_ajax_requests?
    end
  end

  def finished_all_ajax_requests?
    page.evaluate_script('jQuery.active').zero?
  end

  def check_active_menu_and_submenu(menu, submenu)
    check_active_menu(menu)
    check_active_submenu(submenu)
  end
end

RSpec.configure do |config|
  config.include Helpers, type: :request
end
