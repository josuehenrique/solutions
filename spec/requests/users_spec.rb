require 'spec_helper'

feature 'Users' do
  background do
    sign_in
  end

  scenario 'change profile' do
    click_link 'user-menu-settings'

    click_link 'Perfil'

    expect(page).to have_field 'Usuário', with: 'user'

    click_button 'Salvar'

    expect(page).to have_content 'Senha atual não pode ficar em branco'

    fill_in 'Senha atual', with: '12345678'

    click_button 'Salvar'

    expect(page).to have_notice 'Você atualizou sua conta com sucesso'

    click_link 'user-menu-settings'

    click_link 'Perfil'

    expect(page).to have_field 'Usuário', with: 'user'
  end
end
