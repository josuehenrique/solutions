require 'spec_helper'

describe Archive do
  subject { FactoryGirl.cache(:archive) }

  context 'validations' do
    it { should validate_uniqueness_of(:archive_file_name).scoped_to(:directory_id) }
  end
end
