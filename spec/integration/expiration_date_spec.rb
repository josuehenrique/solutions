require 'spec_helper'

describe ExpirationDate do
  it 'closing date must be different from the expiration date if the plan is in arrears' do
    subject.expiration_day = 15
    subject.closing_day = 15
    subject.billing_type = ChargingType::ARREARS
    expect(subject).to_not be_valid
    expect(subject.errors[:base]).to include
    'Para faturamento postecipado, o dia de fechamento deve ser diferente do dia de vencimento.'
  end

  it 'expiration date must be equal to the closing date for early billing' do
    subject.expiration_day = 15
    subject.closing_day = 16
    subject.billing_type = ChargingType::EARLY
    expect(subject).to_not be_valid
    expect(subject.errors[:base]).to include
    'Para faturamento antecipado, o dia de vencimento deve ser igual ao dia de fechamento.'
  end

  it 'expiration date must be equal to the closing date for early billing' do
    subject.expiration_day = 15
    subject.closing_day = 15
    subject.billing_type = ChargingType::EARLY
    subject.classification = AccountPlanDebt::FIXED
    expect(subject).to_not be_valid
    expect(subject.errors[:base]).to include
    'O vencimento fixo não pode ser usado junto com faturamento antecipado.'
  end
end
