require 'spec_helper'

describe ServerServiceDomain do
  subject { FactoryGirl.cache(:server_service_domain) }

  context 'validations' do
    it { should validate_uniqueness_of(:server_service_id).scoped_to(:domain_id) }
  end
end
