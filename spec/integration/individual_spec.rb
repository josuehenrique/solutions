require 'spec_helper'

describe Individual do
  describe 'Uniq CPF' do
    it 'validates CPF as uniq for client' do
      individual = FactoryGirl.cache(:individual_client)

      # Criar um funcionário com o mesmo CPF
      employee = FactoryGirl.build(:individual, cpf: individual.cpf)
      expect(employee.valid?).to eq true

      # Criar outro cliente com o mesmo CPF
      another_individual = individual.dup
      expect(another_individual.valid?).to eq false
      expect(another_individual.errors[:cpf]).to include 'já está em uso'
    end
  end
end
