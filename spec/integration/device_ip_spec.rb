require 'spec_helper'

describe DeviceIp do
  subject do
    FactoryGirl.cache(:radius_server)
    FactoryGirl.cache(:device_ip)
  end

  context 'validations' do
    it { should validate_uniqueness_of(:ip) }
  end
end
