require 'spec_helper'
require 'rake'

describe Permit do
  subject { FactoryGirl.cache(:permit) }

  context 'validations' do
    it { should validate_uniqueness_of(:action).scoped_to([:controller, :modulus]) }
  end

  it 'creates all permits' do
    load File.expand_path('../../../lib/tasks/permit.rake', __FILE__)
    Rake::Task.define_task(:environment)
    Rake::Task['permit:create'].invoke
  end
end
