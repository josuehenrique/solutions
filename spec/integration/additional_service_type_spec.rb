require 'spec_helper'

describe AdditionalServiceType do
  subject { FactoryGirl.cache(:additional_service_type) }

  context 'validations' do
    it { should validate_uniqueness_of(:name) }
  end
end
