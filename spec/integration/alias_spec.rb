require 'spec_helper'

describe Alias do
  subject { FactoryGirl.cache(:alias_setor) }

  context 'validations' do
    it { should validate_uniqueness_of(:destino).scoped_to(:email_id) }
  end
end
