require 'spec_helper'

describe Email do
  subject { FactoryGirl.cache(:email_clienteplano) }

  context 'validations' do
    it { should validate_uniqueness_of(:user).scoped_to(:domain_id) }
  end

  context 'when you exceed the limit per customer email' do
    it 'should not create email' do
      email = FactoryGirl.build(:email_clienteplano, related: subject.related, user: 'jhony')

      subject.related.reload

      expect(email.valid?).to eq false

      expect(email.errors[:base]).to include 'O número máximo de emails para o plano do cliente já foi atingindo'
    end
  end
end
