require 'spec_helper'

describe Bill do
  describe '#payment_way' do
    it 'should return empty (bill not paid)' do
      bill = FactoryGirl.cache(:bill)
      expect(bill.payment_way).to eq ' - '
    end
  end

  describe '#generated_accounts' do
    it 'should return generated accounts' do
      generated_bill = FactoryGirl.cache(:generated_bill).bill
      expect(generated_bill.generated_accounts).to eq [generated_bill]
    end
  end

  describe '#traded_accounts' do
    it 'should return traded accounts' do
      negotiation = FactoryGirl.cache(:negotiation)
      generated_bill = FactoryGirl.cache(:generated_bill, negotiation: negotiation).bill
      traded_bill = FactoryGirl.cache(:traded_bill, negotiation: negotiation).bill
      expect(generated_bill.traded_accounts).to eq [traded_bill]
    end
  end
end
