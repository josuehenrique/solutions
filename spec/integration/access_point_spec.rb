require 'spec_helper'

describe AccessPoint do
  subject { FactoryGirl.cache(:access_point) }

  context 'validations' do
    it { should validate_uniqueness_of(:name) }
  end
end
