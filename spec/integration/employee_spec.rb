require 'spec_helper'

describe Employee do
  describe '#permitted_for?' do
    before { @employee = FactoryGirl.cache(:wendell) }

    it 'checks permissions at business unit' do
      business_unit = FactoryGirl.cache(:business_unit)
      expect(@employee.permitted_for?(business_unit)).to eq false

      FactoryGirl.cache(:employee_relation, employee: @employee, related: business_unit)
      expect(@employee.permitted_for?(business_unit)).to eq true
    end

    it 'checks permissions at stock' do
      stock = FactoryGirl.cache(:stock)
      expect(@employee.permitted_for?(stock)).to eq false

      FactoryGirl.cache(:employee_relation, employee: @employee, related: stock)
      expect(@employee.permitted_for?(stock)).to eq true
    end

    it 'checks permissions at bank_account' do
      bank_account = FactoryGirl.cache(:bank_account)
      expect(@employee.permitted_for?(bank_account)).to eq false

      FactoryGirl.cache(:employee_relation, employee: @employee, related: bank_account)
      expect(@employee.permitted_for?(bank_account)).to eq true
    end

    it 'checks permissions at cashier' do
      cashier = FactoryGirl.cache(:cashier)
      expect(@employee.permitted_for?(cashier)).to eq false

      FactoryGirl.cache(:employee_relation, employee: @employee, related: cashier)
      expect(@employee.permitted_for?(cashier)).to eq true
    end
  end
end
