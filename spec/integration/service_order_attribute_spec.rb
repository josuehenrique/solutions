require 'spec_helper'

describe ServiceOrderAttribute do
  subject { FactoryGirl.cache(:service_order_attribute) }

  context 'validations' do
    it { should validate_uniqueness_of(:os_type_attribute_id).scoped_to(:service_order_id) }
  end
end
