require 'spec_helper'

describe CostCenter do
  subject { FactoryGirl.cache(:cost_center) }

  context 'uniqueness' do
    it { should validate_uniqueness_of(:name).scoped_to(:business_unit_id, :parent_id) }
  end

  it 'has a unit root by business unit' do
    FactoryGirl.cache(:cost_center)
    cost_center = FactoryGirl.build(:cost_center, name: 'Try')

    expect(cost_center.valid?).to eq false
    expect(cost_center.errors[:business_unit_id]).to include 'Já existe um centro de custo (raiz) para essa unidade'
  end

  it 'has the same parents business unit' do
    parent = FactoryGirl.cache(:cost_center)
    cost_center = FactoryGirl.build(
      :cost_center,
      name: 'Try',
      parent: parent,
      business_unit: FactoryGirl.cache(:turbo_relevo)
    )

    expect(cost_center.valid?).to eq false
    expect(cost_center.errors[:business_unit_id]).to include 'A unidade deve ser a mesma do superior'
  end


  it 'analytics element cannot have child' do
    parent = FactoryGirl.cache(
      :cost_center,
      classification: AccountPlanType::ANALYTICS,
      kind: CostCenterType::RMA
    )
    cost_center = FactoryGirl.build(
      :cost_center,
      name: 'Try',
      parent: parent,
    )

    expect(cost_center.valid?).to be_false
    expect(cost_center.errors[:base]).to include 'Analítico não pode ter filhos'
  end
end
