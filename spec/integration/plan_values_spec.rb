require 'spec_helper'

describe PlanValue do

  context 'When you have more than one installation value' do
    it 'Should not create plan registration' do
       plan = FactoryGirl.cache(:plan)
       plan_value = FactoryGirl.build(:plan_value, plan: plan, classification: PlanValueType::INSTALLATION)
       plan_value.valid?
       expect(plan_value.errors[:classification]).to include 'Pode ter apenas um valor de instalação'
    end
  end

   context 'When you have more than one accession value' do
     it 'Should not create plan registration' do
       plan = FactoryGirl.cache(:plan)
       plan_value = FactoryGirl.build(:plan_value, plan: plan, classification: PlanValueType::ACCESSION)
       plan_value.valid?
       expect(plan_value.errors[:classification]).to include 'Pode ter apenas um valor de adesão'
     end
   end
end
