require 'spec_helper'

describe ClientPlanAddress do
  subject { FactoryGirl.cache(:client_plan_address) }

  context 'validations' do
    it { should validate_uniqueness_of(:classification).scoped_to(:client_plan_id) }
  end
end
