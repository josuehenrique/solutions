require 'spec_helper'

describe TemporaryActivation do
  subject { FactoryGirl.cache(:temporary_activation) }

  context 'when the client had a temporary activation in last 30 days' do
    it 'should not created another' do
      situation = FactoryGirl.cache(:situacao_plano_suspenso)
      client_plan = FactoryGirl.cache(:client_plan, client_plan_situation: situation)
      second_activation = FactoryGirl.cache(
        :temporary_activation,
        client_plan: client_plan,
        created_at: Date.current + 15.days
      )

      expect(second_activation.valid?)
      expect(second_activation.errors[:base]).to include 'Já existe ativação temporária com menos de 30 dias.'
    end
  end

  context 'When the customers plan is canceled' do
    it 'Should not create temporary activation' do
      situation = FactoryGirl.cache(:situacao_plano_cancelado)
      client_plan = FactoryGirl.cache(:client_plan, client_plan_situation: situation)
      situacao_plano_cancelado = FactoryGirl.build(:temporary_activation, client_plan: client_plan)

      expect(situacao_plano_cancelado.valid?).to eq false
      expect(situacao_plano_cancelado.errors[:base]).to include 'Plano do cliente não pode estar cancelado.'
    end
  end

  context 'When the customers plan is not suspended ' do
    it 'Should not create temporary activation' do
      situacao_plano_suspenso = FactoryGirl.build(:temporary_activation)

      expect(situacao_plano_suspenso.valid?).to eq false
      expect(situacao_plano_suspenso.errors[:base]).to include 'Não é possível realizar ativação temporária.'
    end
  end

  context 'When the customers plan is suspended and open' do
    it 'Should not create temporary activation' do
      client_plan_suspension = FactoryGirl.cache(:client_plan_suspension)
      temporary_activation = FactoryGirl.build(:temporary_activation, client_plan: client_plan_suspension.client_plan)
      expect( temporary_activation.valid?).to eq false
      expect(temporary_activation.errors[:base]).to include 'Plano suspenso a pedido, não pode ser ativado temporariamente'
    end
  end
end
