require 'spec_helper'

describe Category do
  subject { FactoryGirl.cache(:category) }
  it { should validate_uniqueness_of(:name) }
end
