require 'spec_helper'

describe ScheduledTask do
  describe '#one scheduled task for classification in one client plan' do
    subject { FactoryGirl.cache(:scheduled_task) }

    context 'uniqueness' do
      it { should validate_uniqueness_of(:classification).scoped_to(:client_plan_id) }
      it { should validate_uniqueness_of(:scheduling_date).scoped_to(:client_plan_id) }
    end
  end

  it 'Scheduling date have be greater than or equal today' do
    scheduled_task = FactoryGirl.cache(:scheduled_task)
    scheduled_task.scheduling_date = Date.current

    expect(scheduled_task.valid?).to eq false

    expect(scheduled_task.errors[:scheduling_date]).to include "deve ser depois de #{I18n.l Date.current}"
  end

  describe '#check_situation' do
    it 'On reactivation, choose one active service situation' do
      scheduled_task = FactoryGirl.cache(:scheduled_task)
      scheduled_task.client_plan_situation = FactoryGirl.cache(:situacao_plano_suspenso)

      expect(scheduled_task.valid?).to eq false

      expect(scheduled_task.errors[:base]).to include 'Situação inválida'
    end

    it 'On suspend, choose one bill and service situation' do
      scheduled_task = FactoryGirl.cache(:scheduled_task)
      scheduled_task.classification = ScheduledTaskClassification::SUSPEND

      expect(scheduled_task.valid?).to eq false

      expect(scheduled_task.errors[:base]).to include 'Situação inválida'
    end
  end

  describe '#destroy' do
    it "Cannot destroy if scheduled task's scheduling date is less than today" do
      scheduled_task = FactoryGirl.cache(:scheduled_task)
      scheduled_task.scheduling_date = Date.current - 1.day
      scheduled_task.destroy

      expect(scheduled_task.errors[:base]).to include 'Tarefa Programada não pode ser apagada.'
    end
  end

  describe '#task_executed' do
    it 'Cannot change if scheduled task was executed' do
      scheduled_task = FactoryGirl.cache(:scheduled_task, executed: true)
      scheduled_task.valid?
      expect(scheduled_task.errors[:base]).to include 'A Tarefa Programada já foi executada. Não é possível alterá-la.'
    end
  end
end
