require 'spec_helper'

describe 'Factory Girl Cache' do
  let(:city) { FactoryGirl.cache(:city) }

  it 'do not allow to create the same record twice' do
    expect(FactoryGirl.cache(:city)).to eq city
  end
end
