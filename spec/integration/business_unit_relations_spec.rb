require 'spec_helper'

describe BusinessUnitRelation do
  subject { FactoryGirl.cache(:business_unit_relation) }

  context 'validations' do
    it { should validate_uniqueness_of(:business_unit_id).scoped_to(:related_id, :related_type) }
  end
end
