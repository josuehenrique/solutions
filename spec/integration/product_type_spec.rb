require 'spec_helper'

describe ProductType do
  subject { FactoryGirl.cache(:product_type) }

  describe '#available' do
    it 'returns - when the stock is not informed' do
      expect(subject.available('')).to eq '-'
    end

    context 'individual' do
      it 'returns the quantity' do
        product = FactoryGirl.cache(:product, situation: ProductSituation::AVAILABLE)
        create(:product, situation: ProductSituation::AVAILABLE)
        create(:product, situation: ProductSituation::AVAILABLE)

        expect(product.grouping).to eq false

        expect(subject.available(product.stock_id)).to eq 3
      end
    end

    context 'grouping' do
      it 'returns the quantity' do
        product = FactoryGirl.cache(:product_cable)

        expect(product.grouping).to eq true

        expect(product.product_type.available(product.stock_id)).to eq 100
      end
    end
  end
end
