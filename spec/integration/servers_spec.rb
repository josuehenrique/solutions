require 'spec_helper'

describe Server do
  subject { FactoryGirl.cache(:server) }

  context 'validations' do
    it { should validate_uniqueness_of(:ip).scoped_to(:domain) }
    it { should validate_uniqueness_of(:name).scoped_to(:domain) }
  end
end
