require 'spec_helper'

describe Opportunity do
  subject { FactoryGirl.cache(:opportunity) }

  describe 'checking max plots' do
    it 'should be invalid' do
      subject.adhesion_plots = 2
      expect(subject.valid?).to eq false
      expect(subject.errors[:adhesion_plots]).to include 'não é válido'
    end
  end
end
