require 'spec_helper'

describe OsTypeAttribute do
  subject { FactoryGirl.cache(:os_type_attribute) }

  context 'validations' do
    it { should validate_uniqueness_of(:name).scoped_to(:os_type_id) }
  end
end
