require 'spec_helper'

describe OsType do
  subject { FactoryGirl.cache(:os_type) }

  context 'validations' do
    it { should validate_uniqueness_of(:name) }
  end
end
