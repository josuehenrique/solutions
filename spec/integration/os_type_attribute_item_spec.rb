require 'spec_helper'

describe OsTypeAttributeItem do
  subject { FactoryGirl.cache(:os_type_attribute_item) }

  context 'validations' do
    it { should validate_uniqueness_of(:item).scoped_to(:os_type_attribute_id) }
  end

  describe '#check if the parent is activated' do
    it 'cannot create/update/destroy item when parent is deactivated' do
      subject.os_type_attribute.active = false

      subject.item = 'Hello'

      expect(subject.valid?).to eq false

      expect(subject.errors[:base]).to include 'Não é posssível alterar um item de atributo que está desativado.'
    end
  end
end
