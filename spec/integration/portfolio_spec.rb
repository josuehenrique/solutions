require 'spec_helper'

describe Portfolio do
  subject { FactoryGirl.cache(:portfolio) }

  context 'validations' do
    it { should validate_uniqueness_of(:name) }
  end
end
