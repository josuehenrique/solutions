require 'spec_helper'

describe Attendance do
  subject { FactoryGirl.cache(:waiting_attendance) }

  describe 'finalize' do
    it 'Service orders not finished' do
      FactoryGirl.cache(:service_order, attendance: subject)
      expect(subject.finalize).to eq false
      expect(subject.errors[:base]).to include 'Todas as Ordens de Serviço deste atendimento devem estar finalizadas.'
    end

    it 'Description is blank' do
      subject.description = nil
      expect(subject.finalize).to eq false
      expect(subject.errors[:base]).to include 'A descrição do atendimento ' +
        'deve estar preenchida.'
    end

    it 'Alleged problem is blank' do
      expect(subject.finalize).to eq false
      expect(subject.errors[:base]).to include 'Escolha um problema alegado.'
    end

    it 'Identified problem is blank' do
      expect(subject.finalize).to eq false
      expect(subject.errors[:base]).to include 'Escolha um problema identificado.'
    end

   it 'Solution is blank' do
     expect(subject.finalize).to eq false
     expect(subject.errors[:base]).to include 'Escolha uma solução.'
    end
  end
end
