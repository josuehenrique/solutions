require 'spec_helper'

describe PurchaseOrder do
  subject { FactoryGirl.cache(:purchase_order) }

  context 'When there is no purchase order' do
    it "Can not be deleted purchase order" do
      purchase_order = FactoryGirl.build(:purchase_order, situation: RequisitionSituation::CANCELED)
      purchase_order.destroy
      expect(purchase_order.errors[:base]).to include "Apenas pedidos em situação 'requisitado' podem ser deletados."
    end
  end
end
