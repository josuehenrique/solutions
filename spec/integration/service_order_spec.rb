require 'spec_helper'

describe ServiceOrder do
  describe '#attendance_in_progress' do
    it 'cannot create OS when attendance is finished' do
      attendance = FactoryGirl.cache(:finished_attendance)
      service_order = FactoryGirl.build(:service_order, attendance: attendance)

      expect(service_order.valid?).to eq false
      expect(service_order.errors[:base]).to include 'Não pode ser criada uma OS em atendimento finalizado'
    end
  end

  describe '#employee_access' do
    it 'cannot create OS when emplyee does not have aceess at business unit' do
      service_order = FactoryGirl.build(:service_order)

      expect(service_order.valid?).to eq false
      expect(service_order.errors[:base]).to include 'Você não possui acesso na unidade de negócio'
    end
  end

  describe 'service order limit' do
    it 'cannot create service order if limit is reached' do
      FactoryGirl.cache(:business_unit_os_configuration)
      FactoryGirl.cache(:service_order)
      service_order = FactoryGirl.build(:service_order)

      expect(service_order.valid?).to eq false
      expect(service_order.errors[:base]).to include 'O limite de OS já foi atingido'
    end

    it 'creates service order if limit not reached' do
      FactoryGirl.cache(:business_unit_os_configuration_limit_2)
      FactoryGirl.cache(:service_order)
      service_order = FactoryGirl.build(:service_order)
      service_order.valid?

      expect(service_order.errors[:base]).to_not include 'O limite de OS já foi atingido'
    end

    it 'creates service order if it is without limit' do
      service_order = FactoryGirl.build(:service_order, without_limit: true)
      service_order.valid?

      expect(service_order.errors[:base]).to_not include 'O limite de OS já foi atingido'
    end
  end

  context 'finalize service order' do
    let(:current_employee) { FactoryGirl.cache(:employee) }
    let(:service_order) { FactoryGirl.cache(:approved_service_order) }
    let(:service_order_solution) { FactoryGirl.cache(:service_order_solution) }

    # 3 atributos, 2 são obrigatórios
    let(:required1) { FactoryGirl.cache(:os_type_attribute, os_type: service_order.os_type, required: true) }
    let(:required2) { FactoryGirl.cache(:os_type_attribute_obs, os_type: service_order.os_type, required: true) }
    let(:optional) { FactoryGirl.cache(:os_type_attribute_repair, os_type: service_order.os_type) }

    before do
      required2

      # Valor para os atributos
      FactoryGirl.cache(:service_order_attribute, service_order: service_order, os_type_attribute: required1)
      FactoryGirl.cache(:service_order_attribute, service_order: service_order, os_type_attribute: optional)
    end

    describe '#required_attributes_filled?' do
      it 'returns error if the required attributes are no filled' do
        expect(service_order.finalize(current_employee)).to eq false

        expect(service_order.errors[:base]).to include 'Preencha os atributos obrigatórios da Ordem de Serviço.'

        service_order.reload

        # Ao preencher o outro atributo obrigatório, não terá mais o erro em questão
        FactoryGirl.cache(:service_order_attribute, service_order: service_order, os_type_attribute: required2)

        service_order.reload.valid?

        expect(service_order.errors[:base]).to_not include 'Preencha os atributos obrigatórios da Ordem de Serviço.'
      end
    end

    describe '#service_order_solution_filled?' do
      it 'returns error if the service order solution is not filled' do
        expect(service_order.finalize(current_employee)).to eq false

        expect(service_order.errors[:base]).to include 'Preencha a solução da Ordem de Serviço.'

        service_order.service_order_solution = service_order_solution

        service_order.reload.valid?

        expect(service_order.errors[:base]).to_not include 'Preencha a solução da Ordem de Serviço.'
      end
    end
  end
end
