require 'spec_helper'

describe Stock do
  context 'name should be uniq between active records' do
    subject { FactoryGirl.cache(:stock, business_unit: FactoryGirl.cache(:turbo_relevo)) }
    it { should validate_uniqueness_of(:name).scoped_to(:active, :classification) }
  end

  context 'type should be uniq between lending' do
    subject { FactoryGirl.cache(:stock, classification: StockClassification::LENDING_CUSTOMER, business_unit: FactoryGirl.cache(:turbo_relevo)) }
    it { should validate_uniqueness_of(:classification).scoped_to(:active, :business_unit_id) }
  end
end
