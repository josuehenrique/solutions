require 'spec_helper'

describe Billet do
  describe '#next_document_number' do
    it 'returns the next number' do
      bancobrasil = FactoryGirl.cache(:contabancaria_bancobrasil)

      billet = FactoryGirl.cache(:billet)
      create(:billet, document_number: 12346, bank_account: billet.bank_account)
      create(:billet, document_number: 55, bank_account: bancobrasil)

      expect(
        described_class.next_document_number(billet.bank_account_id)
      ).to eq 12347

      expect(
        described_class.next_document_number(bancobrasil.id)
      ).to eq 56
    end
  end
end

