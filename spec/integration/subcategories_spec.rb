require 'spec_helper'

describe Subcategory do
  subject { FactoryGirl.cache(:product_subcategory) }
  it { should validate_uniqueness_of(:name).scoped_to(:category_id) }
end
