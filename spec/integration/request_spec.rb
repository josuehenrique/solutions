require 'spec_helper'

describe Request do
  subject { FactoryGirl.build(:request) }

  describe 'can_finalize?' do
    it 'Request not finalize without request_solution' do
      subject.situation = CallStatus::FINISHED
      expect(subject.valid?).to eq false
      expect(subject.errors[:base]).to include 'Para finalizar, informe uma solução para o ticket.'
    end

    it 'Request finalize with request_solution' do
      request_solution = FactoryGirl.build(:request_solution)
      subject.stub(request_solution: request_solution)
      expect(subject.valid?).to eq true
    end
  end
end
