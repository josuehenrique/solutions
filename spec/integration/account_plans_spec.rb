require 'spec_helper'

describe AccountPlan do
  subject { FactoryGirl.cache(:account_plan) }

  context 'validations' do
    it { should validate_uniqueness_of(:name) }
  end
end
