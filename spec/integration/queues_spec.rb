require 'spec_helper'

describe RequestQueue do
  subject { FactoryGirl.cache(:request_queue) }

  context 'validations' do
    it { should validate_uniqueness_of(:name) }
  end
end
