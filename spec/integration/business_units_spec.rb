require 'spec_helper'

describe BusinessUnit do
  subject { FactoryGirl.cache(:business_unit) }

  context 'validations' do
    it { should validate_uniqueness_of(:name) }
  end

  it 'apportionment sum from all buniness units cannot be more than 100%' do
    expect(subject.apportionment).to eq 9.99

    business_unit = FactoryGirl.build(:business_unit, name: 'UN', apportionment: 95.00)

    expect(business_unit).to_not be_valid
    expect(business_unit.errors[:apportionment]).to include 'A soma dos rateios das unidades não pode ultrapassar 100%'
  end

  it 'Set ISENTO when IE is not informed' do
    subject.ie = ''
    subject.save

    expect(subject.ie).to eq 'ISENTO'
  end

  it 'municipal enrollment cannot be blank when CNAE is informed' do
    subject.cnae = '12345'

    expect(subject).to_not be_valid
    expect(subject.errors[:inscricao_municipal]).to include 'não pode ficar em branco'
  end
end
