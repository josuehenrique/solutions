require 'spec_helper'

describe Company do
  describe 'Uniq CNPJ' do
    it 'validates CNPJ as uniq for client' do
      company = FactoryGirl.cache(:company_client)

      # Criar outro cliente com o mesmo CNPJ
      another_company = company.dup
      expect(another_company.valid?).to eq false
      expect(another_company.errors[:cnpj]).to include 'já está em uso'
    end
  end
end
