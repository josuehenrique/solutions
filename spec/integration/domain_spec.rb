require 'spec_helper'

describe Domain do
  subject { FactoryGirl.cache(:client_plan_domain) }

  context 'when you exceed the limit per customer domain' do
    it 'should not create domain' do
      domain =  FactoryGirl.build(:client_plan_domain, name: 'telecon.com.br')
      subject.reload
      expect(subject.valid?).to eq false
      expect(subject.errors[:base]).to include 'O número máximo de domínios para o plano do cliente já foi atingindo'
    end
  end
end
