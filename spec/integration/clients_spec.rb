require 'spec_helper'

describe Client do
  subject { FactoryGirl.cache(:client) }

  describe '#any_suspended_client_plan?' do
    context 'When dont have client plans linked to client' do
      it 'Should return true' do
        suspended = FactoryGirl.cache(:situacao_plano_suspenso)
        FactoryGirl.cache(:client_plan, client_plan_situation: suspended, client: subject)
        expect(subject.reload.any_suspended_client_plan?).to eq true
      end

      it 'Should return false' do
        expect(subject.reload.any_suspended_client_plan?).to eq false
      end
    end
  end
end
