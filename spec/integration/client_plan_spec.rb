require 'spec_helper'

describe ClientPlan do
  subject { FactoryGirl.cache(:client_plan, opportunity: FactoryGirl.cache(:opportunity_500k)) }

  describe 'Client plan is valid' do
    it 'When dont have client plans linked to client' do
      expect(subject).to be_valid
      expect(subject.errors[:base]).to eq []
    end

    it 'is valid when client has only plans with initial situation' do
      initial = FactoryGirl.cache(:waiting_installation)
      subject.client_plan_situation = initial

      subject.client.stub(client_plans: [subject])

      expect(subject).to be_valid
      expect(subject.errors[:base]).to eq []
    end

    it 'is valid when client has only cancelled plans' do
      canceled = FactoryGirl.cache(:situacao_plano_cancelado)
      subject.client_plan_situation = canceled

      subject.client.stub(client_plans: [subject])

      expect(subject).to be_valid
      expect(subject.errors[:base]).to eq []
    end
  end

  describe 'Client plan is not valid' do
    it 'When have suspends service client plans linked to client' do
      suspends_service = FactoryGirl.cache(:situacao_plano_suspenso)
      FactoryGirl.cache(:client_plan, client: subject.client, client_plan_situation: suspends_service)

      client_plan2 = FactoryGirl.build(:client_plan, client: subject.client)

      subject.client.reload

      expect(client_plan2).to_not be_valid
      expect(client_plan2.errors[:base]).to include 'Novo plano não pode ser criado. Um ou mais planos do cliente estão suspensos.'
    end

    it 'When have suspends charging of client plan linked to client' do
      suspends_charging = FactoryGirl.cache(
        :situacao_plano_suspenso,
        suspends_service: false,
        suspends_charging: true
      )

      FactoryGirl.cache(:client_plan, client: subject.client, client_plan_situation: suspends_charging)
      client_plan2 = FactoryGirl.build(:client_plan, client: subject.client)

      subject.client.reload

      expect(client_plan2).to_not be_valid
      expect(client_plan2.errors[:base]).to include 'Novo plano não pode ser criado. Um ou mais planos do cliente estão suspensos.'
    end
  end
end
