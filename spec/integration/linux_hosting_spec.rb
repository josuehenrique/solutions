require 'spec_helper'

describe LinuxHosting do
  before do
    client_plan = FactoryGirl.cache(:client_plan)
    @domain = FactoryGirl.cache(:domain, client_plan: client_plan, name: 'telgo.com.br')
  end

  describe '#check_domain' do
    it 'checks if domain is linked to hosting server' do
      linux_hosting = FactoryGirl.build(:linux_hosting, domain: @domain)

      expect(linux_hosting.valid?).to eq false
      expect(linux_hosting.errors[:base]).to include 'Domínio sem vínculo com servidor de hospedagem'

      server = FactoryGirl.cache(:hosting_server)
      linux_hosting.plan_server.update_column(:hosting_server_id, server.id)

      linux_hosting.valid?

      expect(linux_hosting.errors[:base]).to_not include 'Domínio sem vínculo com servidor de hospedagem'
    end
  end

  describe '#check_hosting_quantity' do
    it 'checks if the client plan has reached the quantity limit' do
      linux_hosting = FactoryGirl.build(:linux_hosting, domain: @domain)
      linux_hosting.valid?

      expect(linux_hosting.errors[:base]).to_not include 'O número máximo de hospedagens já foi atingindo'

      linux_hosting.plan_server.update_column(:hosting_accounts, 0)

      expect(linux_hosting.valid?).to eq false

      expect(linux_hosting.errors[:base]).to include 'O número máximo de hospedagens já foi atingindo'
    end
  end

  describe '#check_hosting_quota' do
    it 'checks if the client plan has reached the quota limit' do
      linux_hosting = FactoryGirl.build(:linux_hosting, domain: @domain)
      linux_hosting.valid?

      expect(linux_hosting.errors[:base]).to_not include 'A quota de hospedagem já foi atinginda'

      linux_hosting.plan_server.update_column(:hosting_quota, 0)

      expect(linux_hosting.valid?).to eq false

      expect(linux_hosting.errors[:base]).to include 'A quota de hospedagem já foi atinginda'
    end
  end
end
