require 'spec_helper'

describe Directory do
  subject { FactoryGirl.cache(:directory) }

  context 'validations' do
    it { should validate_uniqueness_of(:name).scoped_to(:related_id, :related_type) }
  end
end
