require 'spec_helper'

describe BillAdjustType do
  subject { FactoryGirl.cache(:bill_adjust_type) }

  context 'validations' do
    it { should validate_uniqueness_of(:name) }
  end
end
