require 'spec_helper'

describe Plan do
  subject { FactoryGirl.cache(:plan) }

  context 'validations' do
    it { should validate_uniqueness_of(:name) }
  end

  context 'Speed' do
    it 'should return the plan speed at database format' do
      expect(subject.speed_description(true)).to eq '500k/1000k'
    end
  end
end
