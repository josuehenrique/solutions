require 'model_helper'
require 'app/models/employee'
require 'app/models/notification'
require 'app/models/role'
require 'app/models/user'

describe User do
  it { should belong_to(:employee) }

  it { should have_many(:roles).dependent(:destroy) }
  it { should have_many(:notifications).dependent(:destroy) }

  it { should validate_presence_of(:login) }
  it { should validate_presence_of(:password) }

  it { should delegate(:business_units).to(:employee).allowing_nil(true) }
  it { should delegate(:request_queues).to(:employee).allowing_nil(true) }
  it { should delegate(:bank_accounts).to(:employee).allowing_nil(true) }
  it { should delegate(:cashiers).to(:employee).allowing_nil(true) }
  it { should delegate(:stocks).to(:employee).allowing_nil(true) }
end
