FactoryGirl.define do
  factory :user do
    password '12345678'
    login 'user'
    administrator false

    factory :admin do
      administrator true
    end

    factory :jack do
      login 'jack'
    end

    factory :max do
      login 'max'
    end

    factory :worker_user do
      administrator true
      association :employee, factory: :wendell
      login 'employee'
    end
  end
end
